(definput sensors analog pcf-8591)

(defoutput lamp digital gpio-18)

(defproc off ()
  (progn
    (setq lamp nil)
    (await-for-then dark? 3000 on)))

(defproc dark? ()
  (> (second sensors) 140))

(defproc on ()
  (progn
    (setq lamp t)
    (await-for-then light? 3000 off)))

(defproc light? ()
  (<= (second sensors) 140))

(now off)