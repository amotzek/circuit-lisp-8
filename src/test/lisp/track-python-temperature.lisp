#|
variable instance contains values from a python dictionary that is
repeatedly printed by the python program ../python/temperature/main.py
to the USB serial /dev/ttyACM0.
|#

(definput instance serial python-dictionary)

(defclass temperature ())

(defmethod to-string ((this temperature))
  t
  (if
    (. this temperature)
    (concatenate "temperature " (. this temperature))
    nil))

(defproc track ()
  (progn
    (log-message (to-string (change-class instance temperature)))
    (after 10000 track)))  #| after 10 seconds call track again |#

(now track)  #| call track now |#
