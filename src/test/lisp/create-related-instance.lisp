(setq broker "...")

(setq client-id "...")

(setq user-name "...")

(setq password "...")

(setq topic-name "related-instance/.../incident")

(defstruct incident (::subject ::data)
  (and
    (string? ::subject)
    (string? ::data)))

(defproc send-incident ()
  (publish-message
    broker
    client-id
    user-name
    password
    topic-name
    (new incident
      "Störung"
      "Hier funktioniert schon wieder was nicht.")  #| message |#
    nil))  #| retain |#

(now send-incident)