(defproc on ()
  (progn
    (log-message "on")
    (after 1000 off)))

(defproc off ()
  (progn
    (log-message "off")
    (after 1000 on)))

(now on)