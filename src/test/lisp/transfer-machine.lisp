(definput ain analog pcf-8591)

(defoutput ena digital gpio-17)

(defoutput out-1 digital gpio-27)

(defoutput out-2 digital gpio-22)

(definput end-position digital gpio-23)

(setq count 0)

(setq broker "mqtts://simplysomethings.de")

(setq client-id "...")

(setq user-name "...")

(setq password "...")

(setq topic-name "value/.../activity-count")

(defproc increase-count ()
  (setq count (+ count 1)))

(defproc publish-count ()
  (go
    (publish-message
      broker
      client-id
      user-name
      password
      topic-name
      count  #| message |#
      t)))  #| retain |#

(defproc is-right? ()
  (or
    (greater? (third ain) (first ain))
    (not end-position)))

(defproc is-left? ()
  (and
    (greater? (second ain) (first ain))
    end-position))

(defproc is-between? ()
  (and
    (less? (second ain) (first ain))
    (less? (third ain) (first ain))
    end-position))

(defproc is-dark? ()
  (and
    (greater? (second ain) (first ain))
    (greater? (third ain) (first ain))))

(defproc is-light? ()
  (and
    (less? (second ain) (first ain))
    (less? (third ain) (first ain))))

(defproc is-left-or-dark? ()
  (or
    (is-left?)
    (is-dark?)))

(defproc start-motor ()
  (setq ena t))

(defproc stop-motor ()
  (setq ena nil))

(defproc set-direction-right ()
  (progn
    (setq out-1 t)
    (setq out-2 nil)))

(defproc set-direction-left ()
  (progn
    (setq out-1 nil)
    (setq out-2 t)))

(defproc drive-right ()
  (progn
    (set-direction-right)
    (priority-now 1 start-motor)
    (only-one-of
      (list
        (await-then is-right? wait-right)
        (after 15000 stop-motor)))))

(defproc wait-right ()
  (progn
    (stop-motor)
    (increase-count)
    (publish-count)
    (only-one-of
      (list
        (after 300000 drive-left)
        (await-for-then is-dark? 30000 park)))))

(defproc drive-left ()
  (progn
    (set-direction-left)
    (priority-now 1 start-motor)
    (only-one-of
      (list
        (await-then is-left-or-dark? wait-left)
        (after 15000 stop-motor)))))

(defproc wait-left ()
  (progn
    (stop-motor)
    (increase-count)
    (publish-count)
    (after 300000 drive-right)))

(defproc park ()
  (progn
    (set-direction-left)
    (now start-motor)
    (after 2000 stop-middle)))

(defproc stop-middle ()
  (progn
    (stop-motor)
    (await-for-then is-light? 30000 drive-right)))

(defproc init ()
  (cond
    ((is-dark?)
      (stop-middle))
    ((is-left?)
      (drive-right))
    ((is-between?)
      (drive-right))
    (t
      (drive-left))))

(now init)
