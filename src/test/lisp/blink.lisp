(defoutput lamp digital gpio-18)

(defproc off ()
  (progn
    (setq lamp nil)
    (after 1000 on)))

(defproc on ()
  (progn
    (setq lamp t)
    (after 1000 off)))

(now off)
