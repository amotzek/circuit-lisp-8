(setq broker "mqtts://simplysomethings.de")

(setq client-id "...")

(setq user-name "...")

(setq password "...")

(setq topic-name "activity/send-self")

(defstruct mail (subject data)
  (and
    (string? subject)
    (string? data)))

(publish-message
  broker
  client-id
  user-name
  password
  topic-name
  (new mail
    "Hallo"
    "Dies ist eine Testmail.")  #| message |#
  nil))  #| retain |#
