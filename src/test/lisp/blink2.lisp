(defoutput lamp-red digital gpio-18)

(defoutput lamp-green digital gpio-23)

(defproc off-red ()
  (progn
    (setq lamp-red nil)
    (after 700 on-red)))

(defproc on-red ()
  (progn
    (setq lamp-red t)
    (after 700 off-red)))

(defproc off-green ()
  (progn
    (setq lamp-green nil)
    (after 900 on-green)))

(defproc on-green ()
  (progn
    (setq lamp-green t)
    (after 900 off-green)))

(now off-red)

(now off-green)
