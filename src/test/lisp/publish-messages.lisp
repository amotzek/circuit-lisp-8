(setq broker "mqtt://broker.hivemq.com")

(setq client-id "")

(setq user-name nil)

(setq password nil)

(setq topic-name "goldberg/00002")

(setq count 1)

(defproc publish-messages ()
  (progn
    (go
      (publish-message
        broker
        client-id
        user-name
        password
        topic-name
        count  #| message |#
        t))  #| retain |#
    (log-message count)
    (setq count (+ count 1))
    (after 3000 publish-messages)))  #| after 3 seconds call publish-messages again |#

(now publish-messages)  #| call publish-messages now |#