#| Sensoren |#

(definput j1 digital gpio-18)

(definput j2 digital gpio-25)

(definput j3 digital gpio-24)

(defproc is-outside? ()
  (and (not j1) j2))

(defproc is-left? ()
  (and (not j1) (not j2)))

(defproc is-right? ()
  (and j1 j2))

(defproc is-light? ()
  j3)

(defproc is-dark? ()
  (not j3))

#| Schrittmotor |#

(defoutput m0 digital gpio-4)

(defoutput m1 digital gpio-17)

(defoutput m2 digital gpio-27)

(defoutput m3 digital gpio-22)

(setq step-count 0)

(setq turn-count 0)

(defproc stop-motor ()
  (progn
    (setq step-count (rem step-count 4))
    (setq m0 nil)
    (setq m1 nil)
    (setq m2 nil)
    (setq m3 nil)))

(defproc step-motor ()
  (progn
    (let
      ((phase (rem step-count 4)))
      (cond
        ((= phase 0)
          (progn
            (setq m3 nil)
            (setq m0 t)))
        ((= phase 1)
          (progn
            (setq m0 nil)
            (setq m1 t)))
        ((= phase 2)
          (progn
            (setq m1 nil)
            (setq m2 t)))
        ((= phase 3)
          (progn
            (setq m2 nil)
            (setq m3 t)))))
    (setq step-count (+ 1 step-count))))

#| Broker |#

(setq broker "mqtt://broker.hivemq.com")

(setq client-id "")

(setq user-name nil)

(setq password nil)

(setq state-topic "goldberg/00000")

(setq turn-count-topic "goldberg/00001")

(defproc publish-turn-count ()
  (go
    (publish-message
      broker
      client-id
      user-name
      password
      turn-count-topic
      turn-count  #| message |#
      t)))  #| retain |#

(defproc publish-darkness ()
  (go
    (publish-message
      broker
      client-id
      user-name
      password
      state-topic
      "darkness"  #| message |#
      t)))  #| retain |#

(defproc publish-light ()
  (go
    (publish-message
      broker
      client-id
      user-name
      password
      state-topic
      "light"  #| message |#
      t)))  #| retain |#

#| Positionserkennung |#

(setq wait-flag nil)

(defproc set-wait ()
  (setq wait-flag t))

(defproc should-wait? ()
  wait-flag)

(defproc reset-wait ()
  (setq wait-flag nil))

(defproc unknown-position ()
  (await-for-then is-outside? 1000 outside-position))

(defproc outside-position ()
  (progn
    (set-wait)
    (await-then is-right? right-position)))

(defproc right-position ()
  (await-then is-left? left-position))

(defproc left-position ()
  (await-for-then is-outside? 3000 outside-position))

#| Motorsteuerung |#

(defproc wait ()
  (progn
    (reset-wait)
    (stop-motor)
    (setq turn-count (+ 1 turn-count))
    (publish-turn-count)
    (only-one-of
      (list
        (after 30000 turn)
        (await-then is-dark? darkness)))))

(defproc turn ()
  (progn
    (step-motor)
    (only-one-of
      (list
        (after 100 turn)
        (await-then is-dark? darkness)
        (priority-await-then 1 should-wait? wait)))))

(defproc darkness ()
  (progn
    (stop-motor)
    (publish-darkness)
    (await-for-then is-light? 30000 light)))

(defproc light ()
  (progn
    (publish-light)
    (now turn)))

#| Start |#

(now unknown-position)

(now wait)