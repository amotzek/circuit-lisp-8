#|
variable instance contains values from a python dictionary that is
repeatedly printed by the python program ../python/environment/main.py
to the USB serial /dev/ttyACM0.
|#

(definput instance serial python-dictionary)

(defclass environment ())

(defmethod to-string ((this environment))
  t
  (if
    (and (. this temperature) (. this light) (. this angle) (. this acceleration))
    (concatenate "temperature " (. this temperature)
                 " light " (. this light)
                 " angle " (. this angle)
                 " acceleration " (. this acceleration))
    nil))

(defproc track ()
  (progn
    (log-message (to-string (change-class instance environment)))
    (after 10000 track)))  #| after 10 seconds call track again |#

(now track)  #| call track now |#
