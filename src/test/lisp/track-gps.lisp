(definput pos-time serial gps)  #| variable pos-time contains GPS position and time |#

(defproc track ()
  (progn
    (log-message pos-time)
    (after 3000 track)))  #| after 3 seconds call track again |#

(now track)  #| call track now |#
