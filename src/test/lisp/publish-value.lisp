(setq broker "...")

(setq client-id "...")

(setq user-name "...")

(setq password "...")

(setq topic-name "value/.../state")

(defproc publish-value ()
  (publish-message
    broker
    client-id
    user-name
    password
    topic-name
    "Egal"  #| message |#
    t))  #| retain |#

(now publish-value)