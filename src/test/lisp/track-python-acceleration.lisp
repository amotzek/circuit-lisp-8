#|
variable instance contains values from a python dictionary that is
repeatedly printed by the python program ../python/acceleration/main.py
to the USB serial /dev/ttyACM0.
|#

(definput instance serial python-dictionary)

(defclass polar-coordinates ())

(defmethod to-string ((this polar-coordinates))
  t
  (if
    (and (. this angle) (. this radius))
    (concatenate "angle " (. this angle) " radius " (. this radius))
    nil))

(defproc track ()
  (progn
    (log-message (to-string (change-class instance polar-coordinates)))
    (after 3000 track)))  #| after 3 seconds call track again |#

(now track)  #| call track now |#
