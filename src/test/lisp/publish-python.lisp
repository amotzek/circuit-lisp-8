(definput instance serial python-dictionary)

(setq broker "mqtt://broker.hivemq.com")

(setq client-id "")

(setq user-name nil)

(setq password nil)

(setq topic-name "goldberg/00003")

(defproc publish-messages ()
  (progn
    (go
      (publish-message
        broker
        client-id
        user-name
        password
        topic-name
        instance  #| message |#
        t))  #| retain |#
    (after 30000 publish-messages)))  #| after 30 seconds call publish-messages again |#

(now publish-messages)  #| call publish-messages now |#
