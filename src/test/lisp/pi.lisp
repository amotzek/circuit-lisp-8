(defproc expt (x e)
  (cond
    ((not (integer? e))
      (throw (quote error) "exponent must be an integer"))
    ((= e 0)
      1)
    ((= e 1)
      x)
    ((< e 0)
      (/ 1 (expt x (- e))))
    (t
      (let
        ((half-e (/ e 2)))
        (if
          (integer? half-e)
          (let
            ((half-expt (expt x half-e)))
            (* half-expt half-expt))
          (* x (expt x (- e 1))))))))

(defproc pi (n)
  (let
    ((p 1)
     (s 0))
    (dotimes (i (* 4 n) (approximate s (expt 1/10 n)))
      (setq s (+ s p p))
      (setq p (* p (+ i 1) (/ (+ i i 3)))))))

(now (lambda () (log-message (pi 1000))))