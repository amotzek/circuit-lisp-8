from math import atan2, sqrt, pi, degrees
from cooperativemultitasking import Tasks
import board
from adafruit_thermistor import Thermistor
from analogio import AnalogIn
from busio import I2C
from adafruit_lis3dh import LIS3DH_I2C, RANGE_2_G, STANDARD_GRAVITY
from neopixel import NeoPixel

# Circuit Playground Express

def track_temperature():
    print({"temperature": thermistor.temperature})
    tasks.after(10000, track_temperature)  # after 10 seconds call track_temperature again

def track_light():
    print({"light": light.value})
    tasks.if_for_then(lambda : not display_active or light.value >= 1000, 1000, track_light)  # when the display is not active or the light value is greater that 1000 for one second call track_light again

def track_acceleration():
    phi, radius = polar_acceleration()
    detect_radius_change(radius)
    if display_active:
        display_polar(phi, radius)
    print({"angle": degrees(phi), "acceleration": radius})
    tasks.after(500, track_acceleration)  # after 500 milliseconds call track_acceleration again

def polar_acceleration():
    x, y, z = accelerometer.acceleration
    x /= STANDARD_GRAVITY
    y /= -STANDARD_GRAVITY
    phi = atan2(x, y)
    if phi < 0.0:
        phi += 2.0 * pi
    radius = sqrt(x * x + y * y)
    return (phi, radius)

def detect_radius_change(radius):
    global radius_changed, last_radius
    radius_changed = abs(radius - last_radius) > 0.05
    if radius_changed:
        last_radius = radius

def display_polar(phi, radius):
    for i in range(0, 12):
        if not (i == 0 or i == 6):
            set_pixel(i, to_color(phi, i * pi / 6.0, radius))
    show_pixels()

def set_pixel(i, c):
    if i > 6:
        i -= 2
    else:
        i -= 1
    pixels[i] = c

def to_color(phi, upsilon, radius):
    intensity = to_intensity(angular_difference(phi, upsilon))
    if intensity == 0.0:
        return (0, 0, 0)
    r, g, b = colors[to_color_index(radius)]
    r = int(r * intensity)
    g = int(g * intensity)
    b = int(b * intensity)
    return (r, g, b)

def to_intensity(delta):
    return max(0.0, 1.0 - delta * 3.0 / pi)  # pixel stays dark if delta greater than 60 degrees

def angular_difference(phi, upsilon):
    delta = upsilon - phi
    return min(abs(delta), abs(2.0 * pi + delta))

def to_color_index(radius):
    return min(int(radius * len(colors)), len(colors) - 1)  # color gets max if radius is greater than 0.75

def show_pixels():
    pixels.show()

def display_on():
    global display_active
    display_active = True
    tasks.if_for_then(lambda : not radius_changed, 10000, display_off)  # when the radius is unchanged for 10 seconds call display_off

def display_off():
    global display_active
    display_active = False
    pixels.fill((0, 0, 0))
    pixels.show()
    tasks.if_then(lambda : radius_changed, display_on)  # when the radius is changed call display_on

tasks = Tasks()
thermistor = Thermistor(board.TEMPERATURE, 10000.0, 10000.0, 25.0, 3950.0)
light = AnalogIn(board.LIGHT)
accelerometer = LIS3DH_I2C(I2C(board.ACCELEROMETER_SCL, board.ACCELEROMETER_SDA), address = 25)
accelerometer.range = RANGE_2_G
pixels = NeoPixel(board.NEOPIXEL, 10, brightness = 0.3)
pixels.fill((0, 0, 0))
pixels.show()
colors = [(32, 0, 0), (32, 32, 0), (0, 32, 0), (0, 0, 32)]
last_radius = 0.0
radius_changed = False
display_active = True

while True:
    try:
        tasks.now(track_acceleration)  # call track_acceleration now
        tasks.now(display_on)  # call display_on now
        tasks.after(100, track_temperature)  # after 100 milliseconds call track_temperature
        tasks.after(200, track_light)  # after 200 milliseconds call track_light
        while True:
            tasks.run()
    except MemoryError:
        tasks.clear()
