from cooperativemultitasking import Tasks
import board
from adafruit_thermistor import Thermistor
from adafruit_dotstar import DotStar

# Gemma MO with thermistor connected to A2

tasks = Tasks()
thermistor = Thermistor(board.A2, 10000.0, 10000.0, 25.0, 3984.0, high_side = False)
dot = DotStar(board.APA102_SCK, board.APA102_MOSI, 1, brightness = 0.3)
gold = [0xd4, 0xaf, 0x37]
black = [0, 0, 0]

def on():
    dot[0] = gold
    dot.show()
    tasks.after(200, off)

def off():
    dot[0] = black
    dot.show()

def track():
    print({"temperature": thermistor.temperature})
    tasks.now(on)
    tasks.after(10000, track)

tasks.now(track)

while True:
    tasks.run()
