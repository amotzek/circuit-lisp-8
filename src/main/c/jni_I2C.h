#include <jni.h>
//
#ifndef _Included_hardware_raspberrypi_I2C
//
#define _Included_hardware_raspberrypi_I2C
/*
 * Class: lisp_circuit_hardware_lowlevel_I2C
 * Method: ioctl
 * Signature: (Ljava/io/FileDescriptor;I[BI)I
 */
JNIEXPORT jint Java_lisp_circuit_hardware_lowlevel_I2C_ioctl(JNIEnv *, jclass, jobject, jint, jbyteArray, jint);
//
#endif