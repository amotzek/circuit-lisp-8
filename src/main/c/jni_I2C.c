#include <jni_I2C.h>
#include <sys/ioctl.h>
//
// compile with gcc -shared -fpic -o lib_i2c.so -I /usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include/ -I /usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include/linux/ -I . jni_I2C.c
//
JNIEXPORT jint JNICALL Java_lisp_circuit_hardware_lowlevel_I2C_ioctl(JNIEnv *env, jclass cs, jobject fdobj, jint request, jbyteArray argp, jint argi)
{
    jclass fdcs = (*env)->GetObjectClass(env, fdobj);
    jfieldID fdid = (*env)->GetFieldID(env, fdcs, "fd", "I");
    jint fd = (*env)->GetIntField(env, fdobj, fdid);
    jint res;
    //
    if (argp == NULL)
    {
        res = ioctl(fd, request, argi);
    }
    else
    {
        jbyte *bytes = (*env)->GetByteArrayElements(env, argp, NULL);
        res = ioctl(fd, request, bytes);
        (*env)->ReleaseByteArrayElements(env, argp, bytes, JNI_ABORT);
    }
    //
    return res;
}
