package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.ByteArrayOutputStream;
/*
 * Created by andreasm on 23.12.16.
 */
final class PublishPacket extends AbstractPacket
{
    private static final int DUPLICATE = 8;
    private static final int QOS_1 = 2;
    private static final int RETAIN = 1;
    //
    private final String topicname;
    private final Integer packetid;
    private final byte[] payload;
    //
    PublishPacket(boolean duplicate, boolean retain, String topicname, Integer packetid, byte[] payload)
    {
        super(PUBLISH, (duplicate ? DUPLICATE : 0) + (packetid != null ? QOS_1 : 0) + (retain ? RETAIN : 0));
        //
        this.topicname = topicname;
        this.packetid = packetid;
        this.payload = payload;
    }
    //
    public String getTopicName()
    {
        return topicname;
    }
    //
    public Integer getPacketId()
    {
        return packetid;
    }
    //
    @Override
    protected byte[] getHeader()
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        writeString(topicname, out);
        //
        if (packetid != null) writeShort(packetid, out);
        //
        return out.toByteArray();
    }
    //
    @Override
    protected byte[] getPayload()
    {
        return payload;
    }
    //
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("publish ");
        builder.append(packetid);
        builder.append(' ');
        builder.append(topicname);
        builder.append(' ');
        //
        try
        {
            builder.append(new String(payload, "UTF-8"));
        }
        catch (Exception ignore)
        {
        }
        //
        return builder.toString();
    }
}
