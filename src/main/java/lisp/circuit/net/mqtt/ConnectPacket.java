package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.ByteArrayOutputStream;
/*
 * Created by Andreas on 22.12.2016.
 */
final class ConnectPacket extends AbstractPacket
{
    private static final String PROTOCOL_NAME = "MQTT";
    private static final int PROTOCOL_LEVEL = 4;
    private static final int CLEAN_SESSION = 2;
    //
    private final String username;
    private final String password;
    private final int keepalive;
    private final String clientid;
    //
    ConnectPacket(String username, String password, int keepalive, String clientid)
    {
        super(CONNECT, (byte) 0);
        //
        if (keepalive < 0 || keepalive > 65535) throw new IllegalArgumentException("keepalive");
        //
        this.username = username;
        this.password = password;
        this.keepalive = keepalive;
        this.clientid = clientid == null ? "" :clientid;
    }
    //
    @Override
    protected byte[] getHeader()
    {
        int connectflags = CLEAN_SESSION;
        //
        if (username != null) connectflags |= 128;
        //
        if (password != null) connectflags |= 64;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        writeString(PROTOCOL_NAME, out);
        out.write(PROTOCOL_LEVEL);
        out.write(connectflags);
        out.write(keepalive >> 8);
        out.write(keepalive & 255);
        //
        return out.toByteArray();
    }
    //
    @Override
    protected byte[] getPayload()
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        writeString(clientid, out);
        //
        if (username != null) writeString(username, out);
        //
        if (password != null) writeString(password, out);
        //
        return out.toByteArray();
    }
    //
    @Override
    public String toString()
    {
        return "connect";
    }
}
