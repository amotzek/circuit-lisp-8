package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Predicate;
/*
 * Created by Andreas on 25.12.2016.
 */
final class BoundedList<E>
{
    private final LinkedList<E> list;
    private final int capacity;
    //
    BoundedList(int capacity)
    {
        list = new LinkedList<>();
        //
        this.capacity = capacity;
    }
    //
    public E removeIf(Predicate<E> filter)
    {
        if (list.isEmpty()) return null;
        //
        Iterator<E> iterator = list.iterator();
        //
        while (iterator.hasNext())
        {
            E element = iterator.next();
            //
            if (filter.test(element))
            {
                iterator.remove();
                //
                return element;
            }
        }
        //
        return null;
    }
    //
    public void add(E element)
    {
        list.addLast(element);
        //
        if (list.size() > capacity) list.removeFirst();
    }
}
