package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
/*
 * Created by Andreas on 23.12.2016.
 */
public final class MQTTBrokerProxy
{
    private static final int KEEPALIVE = 300;
    //
    private final static ConcurrentHashMap<URI, Session> sessionsbyuri = new ConcurrentHashMap<>();
    //
    private final URI uri;
    private final String clientid;
    private final String username;
    private final String password;
    //
    public MQTTBrokerProxy(URI uri, String clientid, String username, String password)
    {
        this.uri = uri;
        this.clientid = clientid;
        this.username = username;
        this.password = password;
    }
    //
    private Session getOrCreateSession() throws IOException
    {
        Session session1 = sessionsbyuri.get(uri);
        //
        if (session1 != null)
        {
            try
            {
                session1.send(new PingRequestPacket());
                //
                if (session1.receivePingResponse() == null) throw new IOException();
                //
                return session1;
            }
            catch (IOException e)
            {
                session1.close();
                sessionsbyuri.remove(uri, session1);
            }
        }
        //
        Session session2 = new Session(uri);
        session2.send(new ConnectPacket(username, password, KEEPALIVE, clientid));
        ConnectAcknowledgementPacket packet = session2.receiveConnectAcknowledgement();
        //
        if (packet == null) throw new IOException("no connect acknowledgement");
        //
        int returncode = packet.getReturnCode();
        //
        switch (returncode)
        {
            case ConnectAcknowledgementPacket.CONNECTION_ACCEPTED: break;
            case ConnectAcknowledgementPacket.UNACCEPTABLE_PROTOTOL_VERSION: throw new IOException("unacceptable protocol version");
            case ConnectAcknowledgementPacket.IDENTIFIER_REJECTED: throw new IOException("identifier rejected");
            case ConnectAcknowledgementPacket.SERVER_UNAVAILABLE: throw new IOException("server unavailable");
            case ConnectAcknowledgementPacket.BAD_USER_NAME_OR_PASSWORD: throw new IOException("bad user name or password");
            case ConnectAcknowledgementPacket.NOT_AUTHORIZED: throw new IOException("not authorized");
            default: throw new IOException("return code " + returncode);
        }
        //
        session1 = sessionsbyuri.putIfAbsent(uri, session2);
        //
        if (session1 == null) return session2;
        //
        try
        {
            session2.send(new DisconnectPacket());  // Race Condition, session2 wieder schließen
            session2.close();
        }
        catch (IOException ignore)
        {
        }
        //
        return session1;
    }
    //
    public void publishMessage(int qos, boolean retain, String topicname, byte[] payload) throws IOException
    {
        switch (qos)
        {
            case 0:
            {
                Session session = getOrCreateSession();
                session.send(new PublishPacket(false, retain, topicname, null, payload));
                //
                return;
            }
            //
            case 1:
            {
                Session session = getOrCreateSession();
                Integer packetid = session.createPacketId();
                session.send(new PublishPacket(false, retain, topicname, packetid, payload));
                int trycount = 0;
                //
                while (trycount < 30 && session.receivePublishAcknowledgement(packetid) == null)
                {
                    session.send(new PublishPacket(true, retain, topicname, packetid, payload));
                    trycount++;
                }
                //
                session.freePacketId(packetid);
                //
                return;
            }
            //
            default: throw new IllegalArgumentException(String.format("qos %s not supported", qos));
        }
    }
    //
    public void subscribeTopics(Collection<String> topicfilters) throws IOException
    {
        Session session = getOrCreateSession();
        Integer packetid = session.createPacketId();
        //
        try
        {
            session.send(new SubscribePacket(topicfilters, packetid));
            SubscribeAcknowledgementPacket packet = session.receiveSubscribeAcknowledgement(packetid);
            //
            if (packet == null) throw new IOException("no subscribe acknowledgement");
            //
            byte[] payload = packet.getPayload();
            int index = 0;
            //
            for (String topicfilter : topicfilters)
            {
                if (payload[index++] == (byte) 0x80) throw new IllegalStateException("subscription failure for topic filter " + topicfilter);
            }
        }
        finally
        {
            session.freePacketId(packetid);
        }
    }
    //
    public byte[] pollMessage(String topicfilter, long timeout) throws IOException
    {
        Session session = getOrCreateSession();
        PublishPacket packet = session.receivePublish(topicfilter, Math.min(timeout, 1000L * KEEPALIVE));
        //
        if (packet == null) return null;
        //
        Integer packetid = packet.getPacketId();
        //
        if (packetid != null) session.send(new PublishAcknowledgementPacket(packetid));
        //
        return packet.getPayload();
    }
    //
    public void close() throws IOException
    {
        Session session = sessionsbyuri.get(uri);
        //
        if (session == null) return;
        //
        try
        {
            session.send(new DisconnectPacket());
        }
        finally
        {
            session.close();
        }
    }
}
