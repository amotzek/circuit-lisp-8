package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
/*
 * Created by Andreas on 23.12.2016.
 */
final class Session
{
    private static final int DELAY = 1000;
    private static final long TIMEOUT = 20000L;
    private static final int BACKLOG = 30;
    private static final int BUFFER_SIZE = 1024;
    //
    private final Socket socket;
    private final InputStream in;
    private final OutputStream out;
    private final Lock receivelock;
    private final Object sendlock;
    private final Object packetlock;
    private final PacketIdFactory packetids;
    private final BoundedList<AbstractPacket> packets;
    //
    Session(URI uri) throws IOException
    {
        socket = createSocket(uri);
        socket.setSoTimeout(DELAY);
        socket.setTcpNoDelay(true);
        socket.setSendBufferSize(BUFFER_SIZE);
        socket.setReceiveBufferSize(BUFFER_SIZE);
        in = new BufferedInputStream(socket.getInputStream(), BUFFER_SIZE);
        out = new BufferedOutputStream(socket.getOutputStream(), BUFFER_SIZE);
        receivelock = new ReentrantLock(true);
        sendlock = new Object();
        packetlock = new Object();
        packetids = new PacketIdFactory();
        packets = new BoundedList<>(BACKLOG);
    }
    //
    private static Socket createSocket(URI uri) throws IOException
    {
        String scheme = uri.getScheme();
        String host = uri.getHost();
        int port = uri.getPort();
        //
        switch (scheme)
        {
            case "mqtt":
                if (port == -1) port = 1883;
                //
                return new Socket(host, port);
            //
            case "mqtts":
            {
                if (port == -1) port = 8883;
                //
                try
                {
                    SSLContext context = SSLContext.getInstance("TLS");
                    SelfSignedTrustManager trustmanager = new SelfSignedTrustManager();
                    TrustManager[] trustmanagers = new TrustManager[] { trustmanager };
                    context.init(null, trustmanagers, null);
                    SocketFactory factory = context.getSocketFactory();
                    //
                    return factory.createSocket(host, port);
                }
                catch (GeneralSecurityException e)
                {
                    throw new IOException(e);
                }
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create socket for %s, scheme must be mqtt or mqtts", uri));
    }
    //
    public Integer createPacketId()
    {
        return packetids.create();
    }
    //
    public void freePacketId(Integer packetid)
    {
        packetids.free(packetid);
    }
    //
    public void send(AbstractPacket packet) throws IOException
    {
        if (socket.isClosed() && packet instanceof DisconnectPacket) return;
        //
        synchronized (sendlock)
        {
            packet.writeTo(out);
        }
    }
    //
    public ConnectAcknowledgementPacket receiveConnectAcknowledgement() throws IOException
    {
        return (ConnectAcknowledgementPacket) receive(packet -> packet instanceof ConnectAcknowledgementPacket, TIMEOUT);
    }
    //
    public PublishAcknowledgementPacket receivePublishAcknowledgement(int packetid) throws IOException
    {
        return (PublishAcknowledgementPacket) receive(packet -> packet instanceof PublishAcknowledgementPacket
                && ((PublishAcknowledgementPacket) packet).getPacketId() == packetid, TIMEOUT);
    }
    //
    public SubscribeAcknowledgementPacket receiveSubscribeAcknowledgement(int packetid) throws IOException
    {
        return (SubscribeAcknowledgementPacket) receive(packet -> packet instanceof SubscribeAcknowledgementPacket
                && ((SubscribeAcknowledgementPacket) packet).getPacketId() == packetid, TIMEOUT);
    }
    //
    public PublishPacket receivePublish(String topicfilter, long timeout) throws IOException
    {
        return (PublishPacket) receive(packet -> packet instanceof PublishPacket
                && ((PublishPacket) packet).getTopicName().equals(topicfilter), timeout);
    }
    //
    public PingResponsePacket receivePingResponse() throws IOException
    {
        return (PingResponsePacket) receive(packet -> packet instanceof PingResponsePacket, TIMEOUT);
    }
    //
    private AbstractPacket receive(Predicate<AbstractPacket> filter, long timeout) throws IOException
    {
        try
        {
            while (timeout >= 0L)
            {
                synchronized (packetlock)
                {
                    AbstractPacket packet = packets.removeIf(filter);
                    //
                    if (packet != null) return packet;
                }
                //
                if (receivelock.tryLock(DELAY, TimeUnit.MILLISECONDS))
                {
                    try
                    {
                        AbstractPacket packet = AbstractPacket.readFrom(in);
                        //
                        if (packet == null) continue;
                        //
                        if (filter.test(packet)) return packet;
                        //
                        synchronized (packetlock)
                        {
                            packets.add(packet);
                        }
                    }
                    catch (SocketTimeoutException ignore)
                    {
                    }
                    finally
                    {
                        receivelock.unlock();
                    }
                }
                //
                timeout -= DELAY;
            }
        }
        catch (InterruptedException ignore)
        {
        }
        //
        return null;
    }
    //
    public void close()
    {
        try
        {
            socket.close();
        }
        catch (IOException ignore)
        {
        }
    }
}
