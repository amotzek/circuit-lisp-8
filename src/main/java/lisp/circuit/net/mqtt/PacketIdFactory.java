package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashSet;
/*
 * Created by Andreas on 25.12.2016.
 */
final class PacketIdFactory
{
    private final HashSet<Integer> ids;
    private int nextid;
    //
    PacketIdFactory()
    {
        ids = new HashSet<>();
    }
    //
    public synchronized Integer create()
    {
        if (nextid == 0 || nextid >= 65536)
        {
            nextid = 1;
        }
        else
        {
            nextid++;
        }
        //
        Integer id = Integer.valueOf(nextid);
        //
        if (ids.add(id)) return id;
        //
        throw new IllegalStateException("duplicate id");
    }
    //
    public synchronized void free(Integer id)
    {
        ids.remove(id);
    }
}
