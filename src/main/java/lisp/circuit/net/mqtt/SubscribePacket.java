package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.ByteArrayOutputStream;
import java.util.Collection;
/*
 * Created by andreasm on 24.12.16.
 */
final class SubscribePacket extends AbstractPacket
{
    private static final int QOS_1 = 1;
    //
    private final Collection<String> topicfilters;
    private final Integer packetid;
    //
    SubscribePacket(Collection<String> topicfilters, Integer packetid)
    {
        super(SUBSCRIBE, 2);
        //
        this.topicfilters = topicfilters;
        this.packetid = packetid;
    }
    //
    @Override
    protected byte[] getHeader()
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write((packetid >> 8) & 255);
        out.write(packetid & 255);
        //
        return out.toByteArray();
    }
    //
    @Override
    protected byte[] getPayload()
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //
        for (String topicfilter : topicfilters)
        {
            writeString(topicfilter, out);
            out.write(QOS_1);
        }
        //
        return out.toByteArray();
    }
    //
    @Override
    public String toString()
    {
        return "subscribe " + packetid;
    }
}
