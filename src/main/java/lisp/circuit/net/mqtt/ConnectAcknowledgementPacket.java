package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by Andreas on 23.12.2016.
 */
final class ConnectAcknowledgementPacket extends AbstractPacket
{
    private static final int SESSION_PRESENT = 1;
    //
    public static final int CONNECTION_ACCEPTED = 0;
    public static final int UNACCEPTABLE_PROTOTOL_VERSION = 1;
    public static final int IDENTIFIER_REJECTED = 2;
    public static final int SERVER_UNAVAILABLE = 3;
    public static final int BAD_USER_NAME_OR_PASSWORD = 4;
    public static final int NOT_AUTHORIZED = 5;
    //
    private final boolean sessionpresent;
    private final int returncode;
    //
    ConnectAcknowledgementPacket(boolean sessionpresent, int returncode)
    {
        super(CONNACK, 0);
        //
        this.sessionpresent = sessionpresent;
        this.returncode = returncode;
    }
    //
    @SuppressWarnings("unused")
    public boolean isSessionPresent()
    {
        return sessionpresent;
    }
    //
    public int getReturnCode()
    {
        return returncode;
    }
    //
    @Override
    protected byte[] getHeader()
    {
        byte[] header = new byte[2];
        //
        if (sessionpresent) header[0] |= SESSION_PRESENT;
        //
        header[1] = (byte) returncode;
        //
        return header;
    }
    //
    @Override
    protected byte[] getPayload()
    {
        return null;
    }
    //
    @Override
    public String toString()
    {
        return "connect acknowledgement";
    }
}
