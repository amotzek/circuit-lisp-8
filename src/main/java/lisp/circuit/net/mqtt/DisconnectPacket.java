package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by Andreas on 23.12.2016.
 */
final class DisconnectPacket extends AbstractPacket
{
    DisconnectPacket()
    {
        super(DISCONNECT, 0);
    }
    //
    @Override
    protected byte[] getHeader()
    {
        return null;
    }
    //
    @Override
    protected byte[] getPayload()
    {
        return null;
    }
    //
    @Override
    public String toString()
    {
        return "disconnect";
    }
}
