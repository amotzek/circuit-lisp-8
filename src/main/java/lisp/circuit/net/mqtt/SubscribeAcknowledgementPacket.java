package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 24.12.16.
 */
final class SubscribeAcknowledgementPacket extends AbstractPacket
{
    private final int packetid;
    private final byte[] payload;
    //
    SubscribeAcknowledgementPacket(int packetid, byte[] payload)
    {
        super(SUBACK, 0);
        //
        this.packetid = packetid;
        this.payload = payload;
    }
    //
    public int getPacketId()
    {
        return packetid;
    }
    //
    @Override
    protected byte[] getHeader()
    {
        byte[] header = new byte[2];
        header[0] = (byte) ((packetid >> 8) & 255);
        header[1] = (byte) (packetid & 255);
        //
        return header;
    }
    //
    @Override
    protected byte[] getPayload()
    {
        return payload;
    }
    //
    @Override
    public String toString()
    {
        return "subscribe acknowledgement " + packetid;
    }
}
