package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.*;
/*
 * Created by Andreas on 22.12.2016.
 */
abstract class AbstractPacket
{
    private static final int MAX_MULTIPLIER = 128 * 128 * 128;
    private static final int MAX_LENGTH = 256 * 1024;
    //
    static final byte CONNECT = 1; // Client request to connect to Server
    static final byte CONNACK = 2; // Connect acknowledgement
    static final byte PUBLISH = 3; // Publish message
    static final byte PUBACK = 4; // Publish acknowledgement
    static final byte SUBSCRIBE = 8; // Client subscribe request
    static final byte SUBACK = 9; // Subscribe acknowledgement
    static final byte PING_REQUEST = 12; // Ping request
    static final byte PING_RESPONSE = 13; // Ping response
    static final byte DISCONNECT = 14; // Client is disconnecting
    //
    private final int type;
    private final int flags;
    //
    protected AbstractPacket(int type, int flags)
    {
        this.type = type;
        this.flags = flags;
    }
    //
    protected abstract byte[] getHeader();
    //
    protected abstract byte[] getPayload();
    //
    public final void writeTo(OutputStream out) throws IOException
    {
        byte[] header = getHeader();
        byte[] payload = getPayload();
        int length = 0;
        //
        if (header != null) length += header.length;
        //
        if (payload != null) length += payload.length;
        //
        out.write(type << 4 | flags & 15);
        //
        while (true)
        {
            int digit = length & 127;
            length >>= 7;
            //
            if (length > 0)
            {
                out.write(digit | 128);
            }
            else
            {
                out.write(digit);
                //
                break;
            }
        }
        //
        if (header != null) out.write(header);
        //
        if (payload != null) out.write(payload);
        //
        out.flush();
    }
    //
    public static AbstractPacket readFrom(InputStream in) throws IOException
    {
        int r = readByte(in);
        int flags = r & 15;
        int type = r >> 4;
        int multiplier = 1;
        int length = 0;
        //
        do
        {
            r = readByte(in);
            length += (r & 127) * multiplier;
            multiplier <<= 7;
            //
            if (multiplier > MAX_MULTIPLIER) throw new IOException("malformed remaining length");
        }
        while ((r & 128) != 0);
        //
        if (length <= MAX_LENGTH)
        {
            switch (type)
            {
                case CONNACK:
                {
                    if (flags != 0) throw new IOException("connection acknowledgement had flags " + flags);
                    //
                    if (length != 2) throw new IOException("connection acknowledgement had length " + length);
                    //
                    r = readByte(in);
                    boolean sessionpresent = (r & 1) == 1;
                    int returncode = in.read();
                    //
                    return new ConnectAcknowledgementPacket(sessionpresent, returncode);
                }
                //
                case PUBLISH:
                {
                    int topiclength = readShort(in);
                    String topicname = readString(topiclength, in);
                    Integer packetid = null;
                    //
                    if ((flags & 6) > 0) packetid = readShort(in);
                    //
                    int payloadlength = length - topiclength - 2;
                    //
                    if (packetid != null) payloadlength -= 2;
                    //
                    byte[] payload = readBytes(payloadlength, in);
                    //
                    return new PublishPacket((flags & 8) > 0, (flags & 1) > 0, topicname, packetid, payload);
                }
                //
                case PUBACK:
                {
                    if (flags != 0) throw new IOException("publish acknowledgement had flags " + flags);
                    //
                    if (length != 2) throw new IOException("publish acknowledgement had length " + length);
                    //
                    int packetid = readShort(in);
                    //
                    return new PublishAcknowledgementPacket(packetid);
                }
                //
                case SUBACK:
                {
                    if (flags != 0) throw new IOException("subscribe acknowledgement had flags " + flags);
                    //
                    int packetid = readShort(in);
                    int payloadlength = length - 2;
                    byte[] payload = readBytes(payloadlength, in);
                    //
                    return new SubscribeAcknowledgementPacket(packetid, payload);
                }
                //
                case PING_RESPONSE:
                {
                    if (flags != 0) throw new IOException("ping response had flags " + flags);
                    //
                    if (length != 0) throw new IOException("ping response had length " + length);
                    //
                    return new PingResponsePacket();
                }
            }
        }
        //
        skipBytes(length, in);
        //
        return null;
    }
    //
    static void writeString(String string, ByteArrayOutputStream out)
    {
        try
        {
            byte[] bytes = string.getBytes("UTF-8");
            int length = bytes.length;
            //
            if (length >= 65536) throw new IllegalArgumentException("string too long");
            //
            writeShort(length, out);
            out.write(bytes);
        }
        catch (IOException e)
        {
            assert false;
        }
    }
    //
    static void writeShort(int value, ByteArrayOutputStream out)
    {
        out.write(value >> 8);
        out.write(value & 255);
    }
    //
    private static String readString(int length, InputStream in) throws IOException
    {
        byte[] bytes = readBytes(length, in);
        //
        return new String(bytes, "UTF-8");
    }
    //
    private static byte[] readBytes(int length, InputStream in) throws IOException
    {
        byte[] bytes = new byte[length];
        int offset = 0;
        //
        while (length > 0)
        {
            int r = in.read(bytes, offset, length);
            //
            if (r < 0) throw new EOFException();
            //
            length -= r;
            offset += r;
        }
        //
        return bytes;
    }
    //
    private static void skipBytes(int length, InputStream in) throws IOException
    {
        byte[] bytes = new byte[1024];
        //
        while (length > 0)
        {
            int r = in.read(bytes, 0, Math.min(1024, length));
            //
            if (r < 0) throw new EOFException();
            //
            length -= r;
        }
    }
    //
    private static int readShort(InputStream in) throws IOException
    {
        int msb = readByte(in);
        int lsb = readByte(in);
        //
        return (msb << 8) + lsb;
    }
    //
    private static int readByte(InputStream in) throws IOException
    {
        int r = in.read();
        //
        if (r < 0) throw new EOFException();
        //
        return r;
    }
}
