package lisp.circuit.net.mqtt;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 23.12.16.
 */
final class PublishAcknowledgementPacket extends AbstractPacket
{
    private final int packetid;
    //
    PublishAcknowledgementPacket(int packetid)
    {
        super(PUBACK, 0);
        //
        this.packetid = packetid;
    }
    //
    public int getPacketId()
    {
        return packetid;
    }
    //
    @Override
    protected byte[] getHeader()
    {
        byte[] header = new byte[2];
        header[0] = (byte) ((packetid >> 8) & 255);
        header[1] = (byte) (packetid & 255);
        //
        return header;
    }
    //
    @Override
    protected byte[] getPayload()
    {
        return null;
    }
    //
    @Override
    public String toString()
    {
        return "publish acknowledgement";
    }
}
