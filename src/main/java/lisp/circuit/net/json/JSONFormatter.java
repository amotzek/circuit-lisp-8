package lisp.circuit.net.json;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.*;
import java.util.Iterator;
/*
 * Created by andreasm on 22.12.16.
 */
public final class JSONFormatter
{
    private final StringBuilder builder;
    //
    public JSONFormatter()
    {
        builder = new StringBuilder();
    }
    //
    public void append(Sexpression value)
    {
        if (value == null)
        {
            builder.append("null");
        }
        else if (value instanceof Rational || value instanceof Chars)
        {
            builder.append(value);
        }
        else if (value instanceof Symbol)
        {
            builder.append('"');
            builder.append(value);
            builder.append('"');
        }
        else if (value instanceof List)
        {
            append((List) value);
        }
        else if (value instanceof RubyStyleObject)
        {
            append((RubyStyleObject) value);
        }
        else
        {
            throw new IllegalArgumentException("cannot serialize " + value);
        }
    }
    //
    private void append(List list)
    {
        builder.append('[');
        boolean first = true;
        //
        while (list != null)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                builder.append(", ");
            }
            //
            Sexpression value = list.first();
            append(value);
            list = list.rest();
        }
        //
        builder.append(']');
    }
    //
    private void append(RubyStyleObject object)
    {
        builder.append('{');
        Iterator<Symbol> keys = object.keyIterator();
        boolean first = true;
        //
        while (keys.hasNext())
        {
            if (first)
            {
                first = false;
            }
            else
            {
                builder.append(", ");
            }
            //
            Symbol key = keys.next();
            builder.append('"');
            builder.append(key.getName());
            builder.append("\":");
            Sexpression value = object.get(key);
            append(value);
        }
        //
        builder.append('}');
    }
    //
    @Override
    public String toString()
    {
        return builder.toString();
    }
    //
    public byte[] toBytes()
    {
        return toString().getBytes();
    }
}
