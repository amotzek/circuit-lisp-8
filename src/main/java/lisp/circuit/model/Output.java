package lisp.circuit.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
/*
 * Created by Andreas on 13.06.2016.
 */
public abstract class Output
{
    private final Symbol name;
    private Sexpression value;
    //
    protected Output(Symbol name)
    {
        this.name = name;
    }
    //
    public final Symbol getName()
    {
        return name;
    }
    //
    public final void setValue(Sexpression value)
    {
        if (isEqual(value, this.value)) return;
        //
        this.value = value;
        valueChanged();
    }
    //
    protected abstract void valueChanged();
    //
    protected final Sexpression getValue()
    {
        return value;
    }
    //
    public abstract void postConstruct();
    //
    public abstract void preDestroy();
    //
    private static boolean isEqual(Sexpression value1, Sexpression value2)
    {
        if (value1 == value2) return true;
        //
        if (value1 == null) return false;
        //
        return value1.equals(value2);
    }
}
