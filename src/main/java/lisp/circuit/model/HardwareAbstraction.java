package lisp.circuit.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.highlevel.*;
import java.util.Collection;
import java.util.HashMap;
/*
 * Created by Andreas on 13.06.2016.
 */
public final class HardwareAbstraction
{
    private final HashMap<Symbol, Input> inputbyname;
    private final HashMap<Symbol, Output> outputbyname;
    //
    public HardwareAbstraction()
    {
        super();
        //
        inputbyname = new HashMap<>();
        outputbyname = new HashMap<>();
    }
    //
    public void addInput(Symbol inputname, Symbol classname, Sexpression argument)
    {
        if (outputbyname.containsKey(inputname)) throw new IllegalArgumentException(String.format("there is already an output with name %s", inputname.getName()));
        //
        if (inputbyname.containsKey(inputname)) throw new IllegalArgumentException(String.format("duplicate input name %s", inputname));
        //
        Input input = null;
        //
        switch (classname.getName())
        {
            case "analog": input = new AnalogInput(inputname, argument); break;
            case "digital": input = new DigitalInput(inputname, argument); break;
            case "serial": input = new SerialInput(inputname, argument); break;
            case "inertial": input = new InertialInput(inputname, argument); break;
        }
        //
        if (input == null) throw new IllegalArgumentException(String.format("cannot create inputs of type %s", classname.getName()));
        //
        inputbyname.put(inputname, input);
    }
    //
    public void addOutput(Symbol outputname, Symbol classname, Sexpression argument)
    {
        if (inputbyname.containsKey(outputname)) throw new IllegalArgumentException(String.format("there is already an input with name %s", outputname.getName()));
        //
        if (outputbyname.containsKey(outputname)) throw new IllegalArgumentException(String.format("duplicate output name %s", outputname));
        //
        Output output = null;
        //
        switch (classname.getName())
        {
            case "analog": output = new AnalogOutput(outputname, argument); break;
            case "digital": output = new DigitalOutput(outputname, argument); break;
            case "frame-buffer": output = new FrameBufferOutput(outputname, argument); break;
        }
        //
        if (output == null) throw new IllegalArgumentException(String.format("cannot create outputs of type %s", classname.getName()));
        //
        outputbyname.put(outputname, output);
    }
    //
    @SuppressWarnings("unused")
    public Input getInput(Symbol name)
    {
        return inputbyname.get(name);
    }
    //
    @SuppressWarnings("unused")
    public Output getOutput(Symbol name)
    {
        return outputbyname.get(name);
    }
    //
    public Collection<Input> queryInputs()
    {
        return inputbyname.values();
    }
    //
    public Collection<Output> queryOutputs()
    {
        return outputbyname.values();
    }
}