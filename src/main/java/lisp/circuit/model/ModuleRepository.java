package lisp.circuit.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.circuit.io.ModuleReader;
import lisp.module.AbstractRepository;
import lisp.module.Module;
import propertyset.VisitorException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
/*
 * Created by Andreas on 11.04.2018.
 */
public final class ModuleRepository extends AbstractRepository
{
    public ModuleRepository()
    {
        super();
    }
    //
    @Override
    protected Module loadModule(URI uri) throws IOException
    {
        String scheme = uri.getScheme();
        InputStream stream;
        //
        switch (scheme)
        {
            case "local": stream = openLocal(uri); break;
            case "https": stream = openHttps(uri); break;
            default: throw new UnsupportedOperationException(String.format("cannot load module from %s, only schemes local and https are supported", uri));
        }
        //
        try
        {
            ModuleReader reader = new ModuleReader();
            reader.readFrom(stream);
            //
            return reader.getModule();
        }
        catch (VisitorException e)
        {
            throw new IOException(e.getMessage());
        }
        finally
        {
            stream.close();
        }
    }
    //
    private static InputStream openLocal(URI uri) throws IOException
    {
        String name = uri.getSchemeSpecificPart();
        String home = System.getProperty("user.home");
        String separator = System.getProperty("file.separator");
        //
        if (name.contains(".") || name.contains("/") || name.contains(separator)) throw new IOException(String.format("%s is not a legal module name", name));
        //
        File file = new File(String.format("%s/Circuit Lisp/%s.module.xml", home, name));
        //
        return new FileInputStream(file);
    }
    //
    private static InputStream openHttps(URI uri) throws IOException
    {
        URL url = uri.toURL();
        //
        return url.openStream();
    }
}
