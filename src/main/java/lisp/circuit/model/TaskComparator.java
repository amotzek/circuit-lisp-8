package lisp.circuit.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Comparator;
/*
 * Created by Andreas on 30.03.2018.
 */
final class TaskComparator implements Comparator<Task>
{
    @Override
    public int compare(Task task1, Task task2)
    {
        int result = Integer.compare(task2.getPriority(), task1.getPriority());
        //
        if (result != 0) return result;
        //
        if (task1.hasGuard() && !task2.hasGuard()) return -1;
        //
        if (!task1.hasGuard() && task2.hasGuard()) return 1;
        //
        return 0;
    }
}
