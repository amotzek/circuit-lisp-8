package lisp.circuit.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Function;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;
/*
 * Created by Andreas on 27.03.2018.
 */
public final class Tasks
{
    private static final Comparator<Task> comparator = new TaskComparator();
    //
    private final PriorityQueue<Task> queue;
    private volatile long current;
    //
    public Tasks()
    {
        queue = new PriorityQueue<>();
        current = System.currentTimeMillis();
    }
    //
    public synchronized boolean isEmpty()
    {
        return queue.isEmpty();
    }
    //
    public Task after(long delay, Function continuation, int priority)
    {
        Task task = new Task(null, continuation, priority);
        task.setWhen(current + delay);
        add(task);
        //
        return task;
    }
    //
    public Task awaitForThen(Function guard, long duration, Function continuation, int priority)
    {
        Task task = new Task(guard, continuation, priority);
        task.setWhen(current);
        task.setDuration(duration);
        add(task);
        //
        return task;
    }
    //
    public synchronized void add(Task task)
    {
        queue.add(task);
    }
    //
    public synchronized Collection<Task> ready() throws InterruptedException
    {
        setCurrent();
        long span = getTimeSpan();
        //
        if (span > 0L)
        {
            Thread.sleep(span);
            //
            return null;
        }
        //
        ArrayList<Task> tasks = new ArrayList<>(4);
        tasks.add(poll());
        //
        while (!isEmpty() && getTimeSpan() <= 0L)
        {
            tasks.add(poll());
        }
        //
        tasks.sort(comparator);
        //
        return tasks;
    }
    //
    private void setCurrent()
    {
        current = System.currentTimeMillis();
    }
    //
    private long getTimeSpan()
    {
        return queue.peek().getWhen() - current;
    }
    //
    private Task poll()
    {
        return queue.poll();
    }
    //
    public synchronized void makeSiblings(Collection<Task> siblings)
    {
        for (Task sibling : siblings)
        {
            sibling.setSiblings(siblings);
        }
    }
    //
    public synchronized void removeSiblings(Task task)
    {
        Collection<Task> siblings = task.getSiblings();
        //
        if (siblings == null) return;
        //
        for (Task sibling : task.getSiblings())
        {
            if (sibling != task) queue.remove(sibling);
        }
    }
}
