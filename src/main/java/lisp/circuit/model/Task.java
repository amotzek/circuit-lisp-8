package lisp.circuit.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Constant;
import lisp.Sexpression;
import java.util.Collection;
/*
 * Created by Andreas on 27.03.2018.
 */
public final class Task extends Constant implements Comparable<Task>
{
    private final Sexpression guard;
    private final Sexpression continuation;
    private final int priority;
    private Collection<Task> siblings;
    private long when;
    private long duration;
    private long remaining;
    //
    Task(Sexpression guard, Sexpression continuation, int priority)
    {
        this.guard = guard;
        this.continuation = continuation;
        this.priority = priority;
    }
    //
    public boolean hasGuard()
    {
        return guard != null;
    }
    //
    public Sexpression getGuard()
    {
        return guard;
    }
    //
    public Sexpression getContinuation()
    {
        return continuation;
    }
    //
    int getPriority()
    {
        return priority;
    }
    //
    Collection<Task> getSiblings()
    {
        return siblings;
    }
    //
    void setSiblings(Collection<Task> siblings)
    {
        this.siblings = siblings;
    }
    //
    long getWhen()
    {
        return when;
    }
    //
    void setWhen(long when)
    {
        this.when = when;
    }
    //
    void setDuration(long duration)
    {
        this.duration = duration;
        this.remaining = duration;
    }
    //
    public boolean decreaseRemaining(long span)
    {
        if (remaining <= 0L) return false;
        //
        when += span;
        remaining -= span;
        //
        return true;
    }
    //
    public void resetRemaining(long span)
    {
        when += span;
        remaining = duration;
    }
    //
    @Override
    public String getType()
    {
        return "task";
    }
    //
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"tasks cannot be read or written\")";
    }
    //
    @Override
    public int compareTo(Task that)
    {
        return Long.compare(this.when, that.when);
    }
}
