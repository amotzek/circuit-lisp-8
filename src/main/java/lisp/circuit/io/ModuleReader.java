package lisp.circuit.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.Module;
import propertyset.ObjectStructure;
import propertyset.PropertySet;
import propertyset.VisitorException;
import xml.propertyset.PropertySetParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
/**
 * Created by andreasm on 07.06.15
 */
public final class ModuleReader
{
    private Module module;
    //
    public ModuleReader()
    {
        super();
    }
    //
    public void readFrom(InputStream stream) throws IOException, VisitorException
    {
        InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
        PropertySetParser parser = new PropertySetParser(reader);
        PropertySet propertyset = parser.getPropertySet();
        ModuleVisitor visitor = new ModuleVisitor();
        ObjectStructure structure = new ObjectStructure(propertyset);
        structure.traverseWith(visitor);
        Collection<Module> modules = visitor.queryModules();
        //
        if (modules.isEmpty()) throw new VisitorException("cannot read module");
        //
        if (modules.size() > 1) throw new VisitorException("read more than one module");
        //
        module = modules.iterator().next();
    }
    //
    public Module getModule()
    {
        return module;
    }
}
