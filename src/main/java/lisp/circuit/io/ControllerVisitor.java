package lisp.circuit.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.ModuleDependency;
import propertyset.PropertySet;
import propertyset.Visitor;
import propertyset.VisitorException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.LinkedList;
/*
 * Created by andreasm on 12.04.18
 */
final class ControllerVisitor implements Visitor, Syntax
{
    private final LinkedList<ModuleDependency> dependencies;
    private String dependencyname;
    private Integer dependencyversion;
    private URI dependencyuri;
    private String setup;
    //
    ControllerVisitor()
    {
        super();
        //
        dependencies = new LinkedList<>();
    }
    //
    Collection<ModuleDependency> queryModuleDependencies()
    {
        return dependencies;
    }
    //
    String getSetup() { return setup; }
    //
    public int enter(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName(CONTROLLER)) return DEPTH_FIRST;
        //
        if (propertyset.hasName(DEPENDENCY)) return DEPTH_FIRST;
        //
        if (propertyset.hasName(NAME))
        {
            dependencyname = propertyset.getValue();
            //
            return LEAF;
        }
        //
        if (propertyset.hasName(URL) || propertyset.hasName(URI))
        {
            dependencyuri = getURIValue(propertyset);
            //
            return LEAF;
        }
        //
        if (propertyset.hasName(MINIMUM_REQUIRED_VERSION))
        {
            dependencyversion = getIntegerValue(propertyset);
            //
            return LEAF;
        }
        //
        if (propertyset.hasName(SETUP))
        {
            setup = propertyset.getValue();
            //
            return LEAF;
        }
        //
        return LEAF;
    }
    //
    public void leave(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName(DEPENDENCY))
        {
            if (dependencyname == null) throw new VisitorException("dependency without name");
            //
            if (dependencyversion == null) throw new VisitorException("dependency without minimum-required-version");
            //
            if (dependencyuri == null) throw new VisitorException("dependency without url and uri");
            //
            ModuleDependency dependency = new ModuleDependency(dependencyname, dependencyversion, dependencyuri, null);
            dependencies.addLast(dependency);
            dependencyname = null;
            dependencyversion = null;
            dependencyuri = null;
        }
        //
        if (propertyset.hasName(CONTROLLER))
        {
            if (setup == null) throw new VisitorException("controller without setup");
        }
    }
    //
    private static Integer getIntegerValue(PropertySet propertyset) throws VisitorException
    {
        try
        {
            return Integer.valueOf(propertyset.getValue());
        }
        catch (NumberFormatException e)
        {
            throw new VisitorException("version is not a valid integer");
        }
    }
    //
    private static URI getURIValue(PropertySet propertyset) throws VisitorException
    {
        try
        {
            String value = propertyset.getValue();
            //
            return new URI(value);
        }
        catch (URISyntaxException e)
        {
            throw new VisitorException("uri is not valid");
        }
    }
}