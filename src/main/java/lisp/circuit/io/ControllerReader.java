package lisp.circuit.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.circuit.combinator.CircuitModule;
import lisp.circuit.controller.Controller;
import lisp.circuit.model.HardwareAbstraction;
import lisp.circuit.model.ModuleRepository;
import lisp.circuit.model.Tasks;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.Environment;
import lisp.environment.io.EnvironmentReader;
import lisp.module.Module;
import lisp.module.ModuleDependency;
import propertyset.ObjectStructure;
import propertyset.PropertySet;
import propertyset.VisitorException;
import xml.propertyset.PropertySetParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
/*
 * Created by Andreas on 27.03.2018.
 */
public final class ControllerReader
{
    private Controller controller;
    //
    @SuppressWarnings("unused")
    public void readFromResource(Class clazz, String name, String charset) throws VisitorException, IOException, CannotEvalException
    {
        URL url = clazz.getResource(name);
        readFrom(url, charset);
    }
    //
    private void readFrom(URL url, String charset) throws VisitorException, IOException, CannotEvalException
    {
        InputStream stream = url.openStream();
        readFrom(stream, charset);
    }
    //
    public void readFrom(InputStream stream, String charset)  throws VisitorException, IOException, CannotEvalException
    {
        InputStreamReader reader = new InputStreamReader(stream, charset);
        readFrom(reader);
    }
    //
    private void readFrom(Reader reader) throws VisitorException, IOException, CannotEvalException
    {
        if (!(reader instanceof BufferedReader)) reader = new BufferedReader(reader);
        //
        Environment environment = EnvironmentFactory.createEnvironment();
        HardwareAbstraction hardware = new HardwareAbstraction();
        Tasks tasks = new Tasks();
        CircuitModule basemodule = new CircuitModule(hardware, tasks);
        basemodule.augmentEnvironment(environment);
        //
        if (isXML(reader))
        {
            PropertySetParser parser = new PropertySetParser(reader);
            PropertySet propertyset = parser.getPropertySet();
            ObjectStructure structure = new ObjectStructure(propertyset);
            ModuleVisitor modulevisitor = new ModuleVisitor();
            ControllerVisitor controllervisitor = new ControllerVisitor();
            structure.traverseWith(modulevisitor);
            structure.traverseWith(controllervisitor);
            ModuleRepository repository = new ModuleRepository();
            //
            for (Module module : modulevisitor.queryModules())
            {
                repository.addModule(module);
            }
            //
            for (ModuleDependency dependency : controllervisitor.queryModuleDependencies())
            {
                repository.addModule(dependency);
            }
            //
            repository.satisfyDependencies();
            environment = repository.createEnvironment(environment);
            EnvironmentReader reader2 = new EnvironmentReader(environment);
            reader2.readFrom(controllervisitor.getSetup());
        }
        else
        {
            EnvironmentReader reader2 = new EnvironmentReader(environment);
            reader2.readFrom(reader);
        }
        //
        controller = new Controller(environment, hardware, tasks);
    }
    //
    private static boolean isXML(Reader reader) throws IOException
    {
        reader.mark(1);
        int first = reader.read();
        reader.reset();
        //
        return '<' == first;
    }
    //
    public Controller getController()
    {
        return controller;
    }
}
