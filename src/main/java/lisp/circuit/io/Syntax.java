package lisp.circuit.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by Andreas on 27.03.2018.
 */
interface Syntax
{
    String CONTROLLER = "controller";
    String SETUP = "setup";
    //
    String MODULE = "module";
    String NAME = "name";
    String VERSION = "version";
    String URL = "url";
    String URI = "uri";
    String DESCRIPTION = "description";
    String EXPORT = "export";
    String EXPORTS = "exports";
    String BODY = "body";
    String DEPENDENCY = "dependency";
    String MINIMUM_REQUIRED_VERSION = "minimum-required-version";
}
