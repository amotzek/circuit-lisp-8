package lisp.circuit.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.Module;
import lisp.module.ModuleDependency;
import propertyset.PropertySet;
import propertyset.Visitor;
import propertyset.VisitorException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.StringTokenizer;
/*
 * Created by andreasm on 07.06.15
 */
final class ModuleVisitor implements Visitor, Syntax
{
    private final LinkedList<Module> modules;
    private final LinkedList<ModuleDependency> dependencies;
    private boolean inmodule;
    private boolean independency;
    private String modulename;
    private Integer moduleversion;
    private URI moduleuri;
    private String moduledescription;
    private LinkedList<String> moduleexports;
    private String modulebody;
    private String dependencyname;
    private Integer dependencyversion;
    private URI dependencyuri;
    //
    ModuleVisitor()
    {
        super();
        //
        modules = new LinkedList<>();
        moduleexports = new LinkedList<>();
        dependencies = new LinkedList<>();
    }
    //
    Collection<Module> queryModules()
    {
        return modules;
    }
    //
    public int enter(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName(MODULE))
        {
            inmodule = true;
            //
            return DEPTH_FIRST;
        }
        //
        if (inmodule)
        {
            if (propertyset.hasName(NAME))
            {
                if (independency)
                {
                    dependencyname = propertyset.getValue();
                }
                else
                {
                    modulename = propertyset.getValue();
                }
                //
                return LEAF;
            }
            //
            if (propertyset.hasName(VERSION))
            {
                moduleversion = getIntegerValue(propertyset);
                //
                return LEAF;
            }
            //
            if (propertyset.hasName(URL) || propertyset.hasName(URI))
            {
                if (independency)
                {
                    dependencyuri = getURIValue(propertyset);
                }
                else
                {
                    moduleuri = getURIValue(propertyset);
                }
                //
                return LEAF;
            }
            if (propertyset.hasName(DESCRIPTION))
            {
                moduledescription = propertyset.getValue();
                //
                return LEAF;
            }
            //
            if (propertyset.hasName(EXPORT))
            {
                moduleexports.addLast(propertyset.getValue());
                //
                return LEAF;
            }
            //
            if (propertyset.hasName(EXPORTS))
            {
                StringTokenizer tokenizer = new StringTokenizer(propertyset.getValue(), " ", false);
                //
                while (tokenizer.hasMoreTokens())
                {
                    moduleexports.addLast(tokenizer.nextToken());
                }
                //
                return LEAF;
            }
            //
            if (propertyset.hasName(BODY))
            {
                modulebody = propertyset.getValue();
                //
                return LEAF;
            }
            //
            if (propertyset.hasName(DEPENDENCY))
            {
                independency = true;
                //
                return DEPTH_FIRST;
            }
            //
            if (propertyset.hasName(MINIMUM_REQUIRED_VERSION))
            {
                dependencyversion = getIntegerValue(propertyset);
                //
                return LEAF;
            }
        }
        //
        return DEPTH_FIRST;
    }
    //
    public void leave(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName(MODULE))
        {
            Module module = new Module(modulename, moduleversion, moduleuri, moduledescription, null, new ArrayList<>(moduleexports), modulebody, new ArrayList<>(dependencies));
            modules.addLast(module);
            modulename = null;
            moduleversion = null;
            moduleuri = null;
            moduledescription = null;
            moduleexports.clear();
            modulebody = null;
            dependencies.clear();
            inmodule = false;
        }
        //
        if (propertyset.hasName(DEPENDENCY))
        {
            if (dependencyname == null) throw new VisitorException("dependency without name");
            //
            if (dependencyversion == null) throw new VisitorException("dependency without minimum-required-version");
            //
            if (dependencyuri == null) throw new VisitorException("dependency without url and uri");
            //
            ModuleDependency dependency = new ModuleDependency(dependencyname, dependencyversion, dependencyuri, null);
            dependencies.addLast(dependency);
            dependencyname = null;
            dependencyversion = null;
            dependencyuri = null;
            independency = false;
        }
    }
    //
    private static Integer getIntegerValue(PropertySet propertyset) throws VisitorException
    {
        try
        {
            return Integer.valueOf(propertyset.getValue());
        }
        catch (NumberFormatException e)
        {
            throw new VisitorException("version is not a valid integer");
        }
    }
    //
    private static URI getURIValue(PropertySet propertyset) throws VisitorException
    {
        try
        {
            String value = propertyset.getValue();
            //
            return new URI(value);
        }
        catch (URISyntaxException e)
        {
            throw new VisitorException("uri is not valid");
        }
    }
}