package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
import java.util.logging.Logger;
/*
 * Created by Andreas on 31.03.2016.
 */
final class LogMessage extends TypeCheckCombinator
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    LogMessage()
    {
        super(0, 1);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments)
    {
        Sexpression argument = arguments[0];
        //
        if (argument != null) logger.info(argument.toString());
        //
        return argument;
    }
    //
    @Override
    public String toString()
    {
        return "log-message";
    }
}