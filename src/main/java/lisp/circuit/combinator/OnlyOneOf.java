package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.circuit.model.Task;
import lisp.circuit.model.Tasks;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
import java.util.ArrayList;
/*
 * Created by Andreas on 28.03.2018.
 */
final class OnlyOneOf extends TypeCheckCombinator
{
    private final Tasks tasks;
    //
    OnlyOneOf(Tasks tasks)
    {
        super(0, 1);
        //
        this.tasks = tasks;
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        List list = getList(arguments, 0);
        int size = List.length(list);
        //
        if (size >= 2)
        {
            ArrayList<Task> siblings = new ArrayList<>(size);
            //
            try
            {
                while (list != null)
                {
                    Task task = (Task) list.first();
                    siblings.add(task);
                    list = list.rest();
                }
            }
            catch (ClassCastException e)
            {
                throw new CannotEvalException("not a task in list passed to only-one-of");
            }
            //
            tasks.makeSiblings(siblings);
        }
        //
        return arguments[0];
    }
    //
    @Override
    public String toString()
    {
        return "only-one-of";
    }
}
