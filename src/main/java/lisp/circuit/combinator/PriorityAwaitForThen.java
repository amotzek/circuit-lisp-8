package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Function;
import lisp.Rational;
import lisp.Sexpression;
import lisp.circuit.model.Tasks;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
/*
 * Created by Andreas on 28.03.2018.
 */
final class PriorityAwaitForThen extends TypeCheckCombinator
{
    private final Tasks tasks;
    //
    PriorityAwaitForThen(Tasks tasks)
    {
        super(0, 4);
        //
        this.tasks = tasks;
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational priority = getRational(arguments, 0);
        Function guard = getFunction(arguments, 1);
        Rational delay = getRational(arguments, 2);
        Function continuation = getFunction(arguments, 3);
        //
        return tasks.awaitForThen(guard, delay.longValue(), continuation, priority.intValue());
    }
    //
    @Override
    public String toString()
    {
        return "priority-await-for-then";
    }
}
