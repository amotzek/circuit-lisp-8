package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Symbol;
import lisp.circuit.concurrent.BoundedExecutor;
import lisp.circuit.model.HardwareAbstraction;
import lisp.circuit.model.Tasks;
import lisp.combinator.ExecutorCombinator;
import lisp.environment.Environment;
/*
 * Created by Andreas on 27.03.2018.
 */
public final class CircuitModule
{
    private final BoundedExecutor EXECUTOR = new BoundedExecutor(16);
    //
    private final HardwareAbstraction hardware;
    private final Tasks tasks;
    //
    public CircuitModule(HardwareAbstraction hardware, Tasks tasks)
    {
        this.hardware = hardware;
        this.tasks = tasks;
    }
    //
    public void augmentEnvironment(Environment environment)
    {
        environment.add(false, Symbol.createSymbol("now"), new Now(tasks));
        environment.add(false, Symbol.createSymbol("after"), new After(tasks));
        environment.add(false, Symbol.createSymbol("await-then"), new AwaitThen(tasks));
        environment.add(false, Symbol.createSymbol("await-for-then"), new AwaitForThen(tasks));
        environment.add(false, Symbol.createSymbol("priority-now"), new PriorityNow(tasks));
        environment.add(false, Symbol.createSymbol("priority-after"), new PriorityAfter(tasks));
        environment.add(false, Symbol.createSymbol("priority-await-then"), new PriorityAwaitThen(tasks));
        environment.add(false, Symbol.createSymbol("priority-await-for-then"), new PriorityAwaitForThen(tasks));
        environment.add(false, Symbol.createSymbol("only-one-of"), new OnlyOneOf(tasks));
        environment.add(false, Symbol.createSymbol("definput"), new DefInput(hardware));
        environment.add(false, Symbol.createSymbol("defoutput"), new DefOutput(hardware));
        environment.add(false, Symbol.createSymbol("publish-message"), new ExecutorCombinator(new PublishMessage(), EXECUTOR));
        environment.add(false, Symbol.createSymbol("log-message"), new ExecutorCombinator(new LogMessage(), EXECUTOR));
        environment.add(false, Symbol.createSymbol("shutdown"), new ExecutorCombinator(new Shutdown(), EXECUTOR));
    }
}
