package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.model.HardwareAbstraction;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
/*
 * Created by Andreas on 28.03.2018.
 */
final class DefOutput extends TypeCheckCombinator
{
    private final HardwareAbstraction hardware;
    //
    DefOutput(HardwareAbstraction hardware)
    {
        super(7, 3);
        //
        this.hardware = hardware;
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Symbol outputname = getSymbol(arguments, 0, false);
        Symbol classname = getSymbol(arguments, 1, false);
        Sexpression argument = arguments[2];
        //
        try
        {
            hardware.addOutput(outputname, classname, argument);
        }
        catch (Exception e)
        {
            throw new CannotEvalException(e.getMessage());
        }
        //
        return outputname;
    }
    //
    @Override
    public String toString()
    {
        return "defoutput";
    }
}
