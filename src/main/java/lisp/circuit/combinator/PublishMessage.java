package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Sexpression;
import lisp.circuit.net.json.JSONFormatter;
import lisp.circuit.net.mqtt.MQTTBrokerProxy;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
/*
 * Created by Andreas on 01.04.2018.
 */
final class PublishMessage extends TypeCheckCombinator
{
    PublishMessage()
    {
        super(0, 7);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        URI destination = getURI(arguments, 0);
        Chars clientid = getChars(arguments, 1);
        Chars username = getChars(arguments, 2, true);
        Chars password = getChars(arguments, 3, true);
        Chars topicname = getChars(arguments, 4);
        JSONFormatter message = getJSONFormatter(arguments, 5);
        Sexpression retain = arguments[6];
        //
        try
        {
            String scheme = destination.getScheme();
            //
            if (scheme == null) throw new CannotEvalException("uri has no scheme");
            //
            switch (scheme)
            {
                case "mqtt":
                case "mqtts":
                    MQTTBrokerProxy broker = new MQTTBrokerProxy(destination, clientid.getString(), getStringOrNull(username), getStringOrNull(password));
                    broker.publishMessage(1, retain != null, topicname.getString(), message.toBytes());
                    //
                    break;
                //
                default: throw new CannotEvalException(String.format("uri scheme is %s but only mqtt and mqtts are supported in publish-message", scheme));
            }
        }
        catch (IOException e)
        {
            String reason = e.getMessage();
            //
            if (reason == null) reason = e.toString();
            //
            throw new CannotEvalException(reason);
        }
        //
        return arguments[5];
    }
    //
    private URI getURI(Sexpression[] arguments, int index) throws CannotEvalException
    {
        try
        {
            return new URI(getChars(arguments, index).getString());
        }
        catch (URISyntaxException e)
        {
            throw new CannotEvalException(createErrorMessage("uri", arguments[index]));
        }
    }
    //
    private JSONFormatter getJSONFormatter(Sexpression[] arguments, int index) throws CannotEvalException
    {
        Sexpression argument = arguments[index];
        //
        try
        {
            JSONFormatter formatter = new JSONFormatter();
            formatter.append(argument);
            //
            return formatter;
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotEvalException(createErrorMessage("json serializable", argument));
        }
    }
    //
    private static String getStringOrNull(Chars chars)
    {
        if (chars == null) return null;
        //
        return chars.getString();
    }
    //
    @Override
    public String toString()
    {
        return "publish-message";
    }
}
