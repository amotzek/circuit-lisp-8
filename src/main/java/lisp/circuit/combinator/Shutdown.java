package lisp.circuit.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
/*
 * Created by andreasm on 07.10.2016.
 */
final class Shutdown extends TypeCheckCombinator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    //
    Shutdown()
    {
        super(0, 0);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        String os = System.getProperty("os.name");
        //
        if (os.contains("nux"))
        {
            try
            {
                ProcessBuilder builder = new ProcessBuilder("/bin/systemctl", "poweroff");
                builder.inheritIO();
                Process process = builder.start();
                int exitvalue = process.waitFor();
                //
                return new Rational(exitvalue);
            }
            catch (Exception e)
            {
                throw new CannotEvalException(ERROR, new Chars(e.getMessage()));
            }
        }
        else
        {
            throw new CannotEvalException(ERROR, new Chars(String.format("cannot shutdown %s", os)));
        }
    }
    //
    @Override
    public String toString()
    {
        return "shutdown";
    }
}
