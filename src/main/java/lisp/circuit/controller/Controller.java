package lisp.circuit.controller;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.model.HardwareAbstraction;
import lisp.circuit.model.Input;
import lisp.circuit.model.Output;
import lisp.circuit.model.Task;
import lisp.circuit.model.Tasks;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
import java.util.Collection;
/*
 * Created by Andreas on 27.03.2018.
 */
public final class Controller
{
    private static final long CYCLE = 100L;
    //
    private final Environment environment;
    private final HardwareAbstraction hardware;
    private final Tasks tasks;
    private Thread shutdownhook;
    //
    public Controller(Environment environment, HardwareAbstraction hardware, Tasks tasks)
    {
        this.environment = environment;
        this.hardware = hardware;
        this.tasks = tasks;
    }
    //
    public Environment getEnvironment()
    {
        return environment;
    }
    //
    public void postConstruct()
    {
        addShutdownHook();
        postConstructInputsOutputs();
    }
    //
    public void preDestroy()
    {
        preDestroyInputsOutputs();
        removeShutdownHook();
    }
    //
    private void addShutdownHook()
    {
        if (shutdownhook == null)
        {
            shutdownhook = new Thread(this::preDestroyInputsOutputs);
            Runtime.getRuntime().addShutdownHook(shutdownhook);
        }
    }
    //
    private void postConstructInputsOutputs()
    {
        hardware.queryInputs().forEach(Input::postConstruct);
        hardware.queryOutputs().forEach(Output::postConstruct);
    }
    //
    private void preDestroyInputsOutputs()
    {
        hardware.queryInputs().forEach(Input::preDestroy);
        hardware.queryOutputs().forEach(Output::preDestroy);
    }
    //
    private void removeShutdownHook()
    {
        if (shutdownhook != null)
        {
            Runtime.getRuntime().removeShutdownHook(shutdownhook);
            shutdownhook = null;
        }
    }
    //
    public boolean hasTasks()
    {
        return !tasks.isEmpty();
    }
    //
    public Task runTask() throws InterruptedException, CannotEvalException
    {
        Collection<Task> readytasks = ready();
        //
        if (readytasks == null) return null;
        //
        readInputs();
        Task selectedtask = selectTask(readytasks);
        giveBackUnselectedTasks(readytasks, selectedtask);
        //
        if (selectedtask != null)
        {
            call(selectedtask.getContinuation());
            writeOutputs();
        }
        //
        return selectedtask;
    }
    //
    private Collection<Task> ready() throws InterruptedException
    {
        return tasks.ready();
    }
    //
    private void readInputs()
    {
        hardware.queryInputs().forEach(this::readInput);
    }
    //
    private void readInput(Input input)
    {
        Symbol name = input.getName();
        Sexpression value = input.getValue();
        environment.add(true, name, value);
    }
    //
    private Task selectTask(Collection<Task> readytasks) throws CannotEvalException
    {
        for (Task task : readytasks)
        {
            if (!task.hasGuard()) return task;
            //
            if (call(task.getGuard()))
            {
                if (!task.decreaseRemaining(CYCLE)) return task;
            }
            else
            {
                task.resetRemaining(CYCLE);
            }
        }
        //
        return null;
    }
    //
    private boolean call(Sexpression function) throws CannotEvalException
    {
        List list = new List(function, null);
        Closure closure = new Closure(environment, list);
        //
        return closure.eval() != null;
    }
    //
    private void giveBackUnselectedTasks(Collection<Task> readytasks, Task selectedtask)
    {
        if (selectedtask == null)
        {
            readytasks.forEach(tasks::add);
            //
            return;
        }
        //
        for (Task task : readytasks)
        {
            if (selectedtask != task) tasks.add(task);
        }
        //
        tasks.removeSiblings(selectedtask);
    }
    //
    private void writeOutputs()
    {
        hardware.queryOutputs().forEach(this::writeOutput);
    }
    //
    private void writeOutput(Output output)
    {
        try
        {
            Symbol name = output.getName();
            Sexpression value = environment.at(name);
            output.setValue(value);
        }
        catch (NotBoundException ignore)
        {
        }
    }
}
