package lisp.circuit.concurrent;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
/*
 * Created by andreasm on 17.05.16.
 */
final class WaitForExecution implements RejectedExecutionHandler
{
    private final long timeout;
    private final TimeUnit timeunit;
    //
    WaitForExecution(long timeout, TimeUnit timeunit)
    {
        super();
        //
        this.timeout = timeout;
        this.timeunit = timeunit;
    }
    //
    @Override
    public void rejectedExecution(Runnable runnable, ThreadPoolExecutor executor)
    {
        if (!executor.isShutdown())
        {
            BlockingQueue<Runnable> queue = executor.getQueue();
            //
            try
            {
                if (queue.offer(runnable, timeout, timeunit)) return;
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
            }
        }
        //
        throw new RejectedExecutionException();
    }
}