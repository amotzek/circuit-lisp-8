package lisp.circuit.concurrent;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
/*
 * Created by andreasm on 07.04.2017.
 */
public final class BoundedExecutor extends ThreadPoolExecutor
{
    public BoundedExecutor(int poolsize)
    {
        super(1, poolsize,
              30L, TimeUnit.SECONDS,
              new SynchronousQueue<>(),
              new HighPriorityThreadFactory(),
              new WaitForExecution(5L, TimeUnit.SECONDS));
    }
}
