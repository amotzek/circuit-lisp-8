package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashMap;
/*
 * Created by andreasm on 14.04.18
 */
public final class PythonDictionary implements LineConsumer
{
    private final HashMap<String, Object> dictionary;
    private Exception exception;
    //
    public PythonDictionary()
    {
        dictionary = new HashMap<>();
    }
    //
    @Override
    public void acceptLine(String line)
    {
        try
        {
            PythonParser parser = new PythonParser(line);
            parser.run();
            //
            synchronized (this)
            {
                dictionary.putAll(parser.getMap());
            }
        }
        catch (Exception ignore)
        {
        }
    }
    //
    @Override
    public void onException(Exception e)
    {
        exception = e;
    }
    //
    public synchronized Object[] readValues()
    {
        if (exception instanceof IllegalStateException) throw (IllegalStateException) exception;
        //
        if (exception != null) throw new IllegalStateException(exception);
        //
        return new Object[] { new HashMap<>(dictionary) };
    }
}
