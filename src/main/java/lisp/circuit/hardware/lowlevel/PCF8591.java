package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.io.RandomAccessFile;
/*
 * Created by Andreas on 26.09.2016.
 */
public final class PCF8591 extends I2C implements ADC, DAC
{
    private static final int I2C_ADDR = 0x48;
    private static final int DA_ACTIVE = 0x40;
    private static final int AUTO_INC = 0x04;
    //
    private static PCF8591 instance;
    //
    public static PCF8591 getInstance()
    {
        if (instance == null) instance = new PCF8591();
        //
        return instance;
    }
    //
    private int aout;
    //
    private PCF8591()
    {
        super();
    }
    //
    @Override
    public void initialize()
    {
        loadLibrary();
    }
    //
    @Override
    public int[] readValues()
    {
        try (RandomAccessFile file = openFile())
        {
            setSlaveAddress(file, I2C_ADDR);
            file.write(new byte[] { DA_ACTIVE | AUTO_INC, (byte) aout });
            file.readByte();
            int ain0 = file.readUnsignedByte();
            int ain1 = file.readUnsignedByte();
            int ain2 = file.readUnsignedByte();
            int ain3 = file.readUnsignedByte();
            //
            return new int[] { ain0, ain1, ain2, ain3 };
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    @Override
    public void writeValues(int[] values)
    {
        aout = values[0];
        //
        try (RandomAccessFile file = openFile())
        {
            setSlaveAddress(file, I2C_ADDR);
            file.write(new byte[] { DA_ACTIVE, (byte) aout });
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
}