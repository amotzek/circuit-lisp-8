package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
/*
 * Created by Andreas on 02.05.2018.
 */
final class TempFileResource
{
    private final String prefix;
    private final String suffix;
    private final String version;
    private final String pathpreference;
    private final String versionpreference;
    private String path;
    //
    TempFileResource(String prefix, String suffix, String version, String pathpreference, String versionpreference)
    {
        this.prefix = prefix;
        this.suffix = suffix;
        this.version = version;
        this.pathpreference = pathpreference;
        this.versionpreference = versionpreference;
    }
    //
    void createFileIfNeeded()
    {
        Preferences root = Preferences.userRoot();
        String lastpath = root.get(pathpreference, null);
        String lastversion = root.get(versionpreference, null);
        //
        if (cannotRead(lastpath) || wrongVersion(lastversion))
        {
            try (InputStream in = I2C.class.getResourceAsStream(prefix + "." + suffix))
            {
                File file = File.createTempFile(prefix, suffix);
                //
                try (OutputStream out = new FileOutputStream(file))
                {
                    for (int content = in.read(); content != -1; content = in.read())
                    {
                        out.write(content);
                    }
                }
                //
                path = file.getAbsolutePath();
                root.put(pathpreference, path);
                root.put(versionpreference, version);
                root.flush();
            }
            catch (IOException e)
            {
                throw new IllegalStateException(e);
            }
            catch (BackingStoreException ignore)
            {
            }
        }
        else
        {
            path = lastpath;
        }
    }
    //
    String getPath()
    {
        return path;
    }
    //
    private static boolean cannotRead(String lastpath)
    {
        if (lastpath == null) return true;
        //
        File file = new File(lastpath);
        //
        return !file.canRead();
    }
    //
    private boolean wrongVersion(String lastversion)
    {
        if (lastversion == null) return true;
        //
        return !lastversion.equals(version);
    }
}
