package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 13.04.18
 */
public final class GPS implements LineConsumer
{
    private static final String DATETIME_TEMPLATE = "20%s-%s-%sT%s:%s:%sZ";
    //
    private String latitude;
    private String longitude;
    private String datetime;
    private Exception exception;
    //
    public GPS()
    {
    }
    //
    @Override
    public void acceptLine(String line)
    {
        // $GPRMC,190421.00,A,5020.99476,N,00735.53851,E,0.341,,130418,,,A*7F
        if (!line.startsWith("$GPRMC")) return;
        //
        if (!checkSum(line)) return;
        //
        String[] tokens = line.split(",");
        //
        synchronized (this)
        {
            try
            {
                if (!"A".equals(tokens[2])) return;
                //
                setValues(toDegrees(tokens[3]) + tokens[4],
                          toDegrees(tokens[5]) + tokens[6],
                          toDateTime(tokens[9], tokens[1]));
            }
            catch (Exception ignore)
            {
            }
        }
    }
    //
    private void setValues(String latitude, String longitude, String datetime)
    {
        this.latitude = latitude;
        this.longitude = longitude;
        this.datetime = datetime;
    }
    //
    private static boolean checkSum(String line)
    {
        int start = line.indexOf('$');
        //
        if (start < 0) return false;
        //
        int end = line.lastIndexOf('*');
        //
        if (end < 0) return false;
        //
        if (end > line.length() - 2) return false;
        //
        int sum = 0;
        //
        for (int i = start + 1; i < end; i++)
        {
            sum ^= line.charAt(i);
        }
        //
        try
        {
            return Integer.valueOf(line.substring(end + 1), 16) == sum;
        }
        catch (NumberFormatException ignore)
        {
        }
        //
        return false;
    }
    //
    private static String toDegrees(String degreesminutes)
    {
        int idot = degreesminutes.indexOf('.');
        String degrees = degreesminutes.substring(0, idot - 2);
        String minutes = degreesminutes.substring(idot - 2);
        //
        return Double.toString(Double.valueOf(degrees) + Double.valueOf(minutes) / 60.0d);
    }
    //
    private static String toDateTime(String date, String time)
    {
        return String.format(DATETIME_TEMPLATE,
                date.substring(4, 6),
                date.substring(2, 4),
                date.substring(0, 2),
                time.substring(0, 2),
                time.substring(2, 4),
                time.substring(4, 6));
    }
    //
    @Override
    public synchronized void onException(Exception e)
    {
        exception = e;
    }
    //
    public synchronized Object[] readValues()
    {
        if (exception instanceof IllegalStateException) throw (IllegalStateException) exception;
        //
        if (exception != null) throw new IllegalStateException(exception);
        //
        if (latitude == null || longitude == null || datetime == null) return null;
        //
        return new Object[] { latitude, longitude, datetime };
    }
}
