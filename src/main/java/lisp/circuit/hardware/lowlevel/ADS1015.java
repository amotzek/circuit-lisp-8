package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.io.RandomAccessFile;
/*
 * Created by Andreas on 29.3.2018.
 */
public final class ADS1015 extends I2C implements ADC
{
    private static final int I2C_ADDR = 0x48;
    private static final int CONTROL_REG = 0x00;
    private static final int CONVERSION_REG = 0x01;
    private static final int START_SINGLE_CONVERSION = 0x80;
    private static final int SINGLE_ENDED = 0x40;
    private static final int SINGLE_SHOT_MODE = 0x01;
    private static final int DISABLE_COMPARATOR = 0x03;
    //
    private static ADS1015[] adcbyinput = new ADS1015[4];
    //
    public static ADS1015 getInstance(int input)
    {
        if (adcbyinput[input] == null) adcbyinput[input] = new ADS1015(input);
        //
        return adcbyinput[input];
    }
    //
    private final int input;
    //
    private ADS1015(int input)
    {
        this.input = input;
    }
    //
    @Override
    public void initialize()
    {
        loadLibrary();
    }
    //
    @Override
    public int[] readValues()
    {
        try (RandomAccessFile file = openFile())
        {
            setSlaveAddress(file, I2C_ADDR);
            file.write(new byte[] { CONTROL_REG, (byte) (START_SINGLE_CONVERSION | SINGLE_ENDED | input << 6 | SINGLE_SHOT_MODE), DISABLE_COMPARATOR });
            file.write(CONVERSION_REG);
            int ain = file.readShort() >>> 3;
            //
            return new int[] { ain };
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
}