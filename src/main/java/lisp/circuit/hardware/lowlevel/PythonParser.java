package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/*
 * Created by andreasm on 14.04.18
 */
final class PythonParser
{
    private static final Object[] EMPTY_LIST = new Object[0];
    //
    private final String string;
    private int position;
    private Object object;
    //
    PythonParser(String string)
    {
        this.string = string;
    }
    //
    void run()
    {
        skipSpace();
        object = parseAny();
    }
    //
    private Object parseAny()
    {
        switch (current())
        {
            case '\'': return parseString('\'');
            case '"': return parseString('"');
            case '[': return parseList(']');
            case '(': return parseList(')');
            case '{': return parseDictionary();
            case '-':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': return parseNumber();
        }
        //
        throw new IllegalStateException(String.format("cannot parse %s", rest()));
    }
    //
    private String parseString(char delimiter)
    {
        StringBuilder builder = new StringBuilder();
        boolean escape = false;
        next();
        //
        while (true)
        {
            char current = current();
            next();
            //
            if (escape)
            {
                builder.append(current);
                escape = false;
            }
            else if (current == '\\')
            {
                escape = true;
            }
            else if (current == delimiter)
            {
                return builder.toString();
            }
            else
            {
                builder.append(current);
            }
        }
    }
    //
    private Object[] parseList(char delimiter)
    {
        next();
        skipSpace();
        char current = current();
        //
        if (current == delimiter)
        {
            next();
            //
            return EMPTY_LIST;
        }
        //
        ArrayList<Object> list = new ArrayList<>();
        //
        while (true)
        {
            list.add(parseAny());
            skipSpace();
            current = current();
            //
            if (current == delimiter)
            {
                next();
                //
                return list.toArray();
            }
            //
            if (current != ',') throw new IllegalStateException(String.format("expected , or %s in list %s", delimiter, rest()));
            //
            next();
            skipSpace();
        }
    }
    //
    private Map<String, Object> parseDictionary()
    {
        next();
        skipSpace();
        char current = current();
        //
        if (current == '}')
        {
            next();
            //
            return Collections.emptyMap();
        }
        //
        HashMap<String, Object> map = new HashMap<>();
        //
        while (true)
        {
            Object key = parseAny();
            skipSpace();
            current = current();
            //
            if (current != ':') throw new IllegalStateException(String.format("expected : in dictionary %s", rest()));
            //
            next();
            skipSpace();
            Object value = parseAny();
            map.put(key.toString(), value);
            skipSpace();
            current = current();
            //
            if (current == '}')
            {
                next();
                //
                return map;
            }
            //
            if (current != ',') throw new IllegalStateException(String.format("expected , or } in dictionary %s", rest()));
            //
            next();
            skipSpace();
        }
    }
    //
    private Object parseNumber()
    {
        StringBuilder builder = new StringBuilder();
        char current = current();
        //
        do
        {
            builder.append(current);
            next();
            current = current();
        }
        while (!isSpace(current) && !isDelimiter(current));
        //
        String string = builder.toString();
        //
        if (string.indexOf('.') >= 0) return Double.parseDouble(string);
        //
        if (string.length() <= 9) return Integer.parseInt(string);
        //
        if (string.length() <= 18) return Long.parseLong(string);
        //
        return new BigInteger(string);
    }
    //
    private char current()
    {
        if (position < string.length()) return string.charAt(position);
        //
        throw new IllegalStateException("end of string reached");
    }
    //
    private String rest()
    {
        return string.substring(position);
    }
    //
    private void next()
    {
        position++;
    }
    //
    private void skipSpace()
    {
        while (isSpace(current()))
        {
            next();
        }
    }
    //
    private static boolean isSpace(char c)
    {
        return c <= ' ';
    }
    //
    private static boolean isDelimiter(char c)
    {
        return "]),}:".indexOf(c) >= 0;
    }
    //
    @SuppressWarnings("unchecked")
    Map<String, Object> getMap()
    {
        if (object instanceof Map) return (Map<String, Object>) object;
        //
        throw new IllegalStateException(String.format("not a map, but %s", object));
    }
}
