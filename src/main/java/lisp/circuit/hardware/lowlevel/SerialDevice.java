package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.EOFException;
import java.io.IOException;
/*
 * Created by Andreas on 02.05.2018.
 */
public abstract class SerialDevice
{
    private final String name;
    private volatile Thread thread;
    //
    SerialDevice(String name)
    {
        this.name = name;
    }
    //
    final String getName()
    {
        return name;
    }
    //
    public abstract void open();
    //
    abstract boolean isOpen();
    //
    abstract int read() throws IOException;
    //
    abstract void write(char c) throws IOException;
    //
    public final void readLinesInto(LineConsumer consumer)
    {
        if (thread != null) throw new IllegalStateException(String.format("%s is already read", name));
        //
        thread = new Thread(() -> {
            while (isOpen())
            {
                try
                {
                    StringBuilder builder = new StringBuilder();
                    //
                    while (true)
                    {
                        int c = read();
                        //
                        if (c < 0) throw new EOFException(String.format("%s disconnected", name));
                        //
                        if (c == 10) break;
                        //
                        builder.append((char) c);
                    }
                    //
                    consumer.acceptLine(builder.toString());
                }
                catch (Exception e)
                {
                    consumer.onException(e);
                    //
                    break;
                }
            }
        });
        //
        thread.setDaemon(true);
        thread.setName(name);
        thread.start();
    }
    //
    public final void writeLine(String line)
    {
        try
        {
            for (int i = 0; i < line.length(); i++)
            {
                write(line.charAt(i));
            }
            //
            write('\n');
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    public void close()
    {
        if (thread != null)
        {
            try
            {
                thread.interrupt();
                thread.join(3000L);
                thread = null;
            }
            catch (Exception e)
            {
                throw new IllegalStateException(e);
            }
        }
    }
}
