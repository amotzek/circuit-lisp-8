package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
/*
 * Created by andreasm on 13.04.18
 */
public final class TTY extends SerialDevice
{
    private static final String FILE_NAME = "/dev/tty%s";
    //
    private static HashMap<String, TTY> serialbyname = new HashMap<>();
    //
    public static TTY getInstance(String name)
    {
        return serialbyname.computeIfAbsent(name, TTY::new);
    }
    //
    private volatile RandomAccessFile file;
    //
    private TTY(String name)
    {
        super(name);
    }
    //
    public void open()
    {
        if (file != null) return;
        // Vorbedingung: sudo usermod -a -G dialout {user}
        try
        {
            file = new RandomAccessFile(String.format(FILE_NAME, getName()), "rw");
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    boolean isOpen()
    {
        return file != null;
    }
    //
    int read() throws IOException
    {
        return file.read();
    }
    //
    void write(char c) throws IOException
    {
        file.writeChar(c);
    }
    //
    public void close()
    {
        if (file != null)
        {
            try
            {
                file.close();
                file = null;
            }
            catch (Exception e)
            {
                throw new IllegalStateException(e);
            }
        }
        //
        super.close();
    }
}
