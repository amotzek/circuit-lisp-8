package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
/*
 * Created by Andreas on 02.05.2018.
 */
public final class COM extends SerialDevice
{
    private static HashMap<String, COM> serialbyname = new HashMap<>();
    //
    public static COM getInstance(String name)
    {
        return serialbyname.computeIfAbsent(name, COM::new);
    }
    //
    private volatile Process process;
    private volatile InputStream instream;
    private volatile OutputStream outstream;
    //
    private COM(String name)
    {
        super(name);
    }
    //
    public void open()
    {
        TempFileResource resource = new TempFileResource("serialporttoconsole", "exe", "1", "Circuit_Lisp_serialporttoconsole_exe_path", "Circuit_Lisp_serialporttoconsole_exe_version");
        resource.createFileIfNeeded();
        //
        try
        {
            ProcessBuilder builder = new ProcessBuilder(resource.getPath(), getName());
            process = builder.start();
            instream = process.getInputStream();
            outstream = process.getOutputStream();
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    boolean isOpen()
    {
        return process.isAlive();
    }
    //
    int read() throws IOException
    {
        return instream.read();
    }
    //
    void write(char c) throws IOException
    {
        outstream.write(c);
    }
    //
    public void close()
    {
        if (process != null)
        {
            process.destroy();
            process = null;
        }
        //
        if (instream != null)
        {
            try
            {
                instream.close();
                instream = null;
            }
            catch (IOException ignore)
            {
            }
        }
        //
        if (outstream != null)
        {
            try
            {
                outstream.close();
                outstream = null;
            }
            catch (IOException ignore)
            {
            }
        }
        //
        super.close();
    }
}