package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
/*
 * Created by Andreas on 26.09.2016.
 */
abstract class I2C
{
    private static final String FILE_NAME = "/dev/i2c-1";
    private static final int I2C_SLAVE = 0x0703;
    //
    static RandomAccessFile openFile() throws IOException
    {
        return new RandomAccessFile(FILE_NAME, "rw");
    }
    //
    static void setSlaveAddress(RandomAccessFile file, int address) throws IOException
    {
        FileDescriptor fd = file.getFD();
        int res = ioctl(fd, I2C_SLAVE, null, address);
        //
        if (res < 0) throw new IOException(String.format("ioctl returned %s", res));
    }
    //
    private native static int ioctl(FileDescriptor fd, int request, byte[] argp, int argi);
    //
    private static boolean libraryloaded;
    //
    static synchronized void loadLibrary()
    {
        if (libraryloaded) return;
        //
        TempFileResource resource = new TempFileResource("lib_i2c", "so", "1", "Circuit_Lisp_lib_i2c_path", "Circuit_Lisp_lib_i2c_version");
        resource.createFileIfNeeded();
        System.load(resource.getPath());
        libraryloaded = true;
    }
}
