package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.io.RandomAccessFile;
/*
 * Created by Andreas on 24.5.2018.
 */
public final class LSM9DS1 extends I2C implements IMU
{
    private static final int I2C_ADDR = 0x6B;
    private static final int CONTROL_REG_8 = 0x22;
    private static final int CONTROL_REG_1_G = 0x10;
    private static final int CONTROL_REG_5_XL = 0x1f;
    private static final int OUT_REG_X_L_XL = 0x28;
    private static final int OUT_REG_X_L_G = 0x18;
    private static final int SW_RESET = 0x01;
    private static final int IF_ADD_INC = 0x04;
    private static final int ODR_952 = 0xc0;
    private static final int XYZ_EN = 0x38;
    //
    private static LSM9DS1 instance;
    //
    public static LSM9DS1 getInstance()
    {
        if (instance == null) instance = new LSM9DS1();
        //
        return instance;
    }
    //
    private LSM9DS1()
    {
    }
    //
    @Override
    public void initialize()
    {
        loadLibrary();
        reset();
        enable();
    }
    //
    @Override
    public int[] readValues()
    {
        try (RandomAccessFile file = openFile())
        {
            setSlaveAddress(file, I2C_ADDR);
            file.write(new byte[] { (byte) OUT_REG_X_L_XL });
            int ax = file.readUnsignedShort();
            int ay = file.readUnsignedShort();
            int az = file.readUnsignedShort();
            file.write(new byte[] { (byte) OUT_REG_X_L_G });
            int gx = file.readUnsignedShort();
            int gy = file.readUnsignedShort();
            int gz = file.readUnsignedShort();
            //
            return new int[] { ax, ay, az, gx, gy, gz };
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    private static void reset()
    {
        try (RandomAccessFile file = openFile())
        {
            setSlaveAddress(file, I2C_ADDR);
            file.write(new byte[] { CONTROL_REG_8, (byte) (SW_RESET | IF_ADD_INC) });
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
        //
        try
        {
            Thread.sleep(100L);
        }
        catch (InterruptedException e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    private static void enable()
    {
        try (RandomAccessFile file = openFile())
        {
            setSlaveAddress(file, I2C_ADDR);
            file.write(new byte[] { CONTROL_REG_1_G, (byte) ODR_952 });
            file.write(new byte[] { CONTROL_REG_5_XL, (byte) XYZ_EN });
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
}