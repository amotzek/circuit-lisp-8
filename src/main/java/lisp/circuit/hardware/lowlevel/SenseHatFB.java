package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
/*
 * Created by Andreas on 28.09.2016.
 */
public final class SenseHatFB implements FB
{
    private static final String GRAPHICS_FILE_NAME = "/sys/class/graphics/";
    private static final String NAME_FILE_NAME = "name";
    private static final String DEV_DIRECTORY_NAME = "/dev";
    private static final String FB_PREFIX = "fb";
    private static final String FRAME_BUFFER_NAME = "RPi-Sense FB";
    //
    private static SenseHatFB instance;
    //
    public static SenseHatFB getInstance()
    {
        if (instance == null) instance = new SenseHatFB();
        //
        return instance;
    }
    //
    private File framebuffer;
    //
    private SenseHatFB()
    {
    }
    //
    @Override
    public void initialize()
    {
        if (framebuffer == null) framebuffer = findDevice();
    }
    //
    @Override
    public int size()
    {
        return 128;
    }
    //
    @Override
    public void setPixel(int x, int y, int w, byte[] pixels)
    {
        setPixel(x, y, w, w, w, pixels);
    }
    //
    @Override
    public void setPixel(int x, int y, int r, int g, int b, byte[] pixels)
    {
        if (x < 0 || x > 7 || y < 0 || y > 7) return;
        //
        if (r < 0) r = 0;
        //
        if (r > 255) r = 255;
        //
        if (g < 0) g = 0;
        //
        if (g > 255) g = 255;
        //
        if (b < 0) b = 0;
        //
        if (b > 255) b = 255;
        //
        int bits16 = (((r >> 3) & 0x1F) << 11)
                + (((g >> 2) & 0x3F) << 5)
                + ((b >> 3) & 0x1F);
        //
        int index = 2 * x + 16 * y;
        pixels[index] = (byte) (bits16 & 0xFF);
        pixels[index + 1] = (byte) ((bits16 >> 8) & 0xFF);
    }
    //
    @Override
    public void showPixels(byte[] pixels)
    {
        try (RandomAccessFile file = openFile())
        {
            file.seek(0L);
            file.write(pixels);
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e);
        }
    }
    //
    private RandomAccessFile openFile() throws IOException
    {
        return new RandomAccessFile(framebuffer, "rw");
    }
    //
    private static File findDevice()
    {
        File graphics = new File(GRAPHICS_FILE_NAME);
        File[] fbs = graphics.listFiles((dir, name) -> name.startsWith(FB_PREFIX));
        //
        if (fbs != null)
        {
            for (File fb : fbs)
            {
                File fbname = new File(fb, NAME_FILE_NAME);
                //
                if (!fbname.isFile()) continue;
                //
                try (FileReader reader = new FileReader(fbname))
                {
                    char[] buffer = new char[128];
                    int count = reader.read(buffer);
                    //
                    if (count <= 0) continue;
                    //
                    String content = new String(buffer, 0, count);
                    //
                    if (!content.contains(FRAME_BUFFER_NAME)) continue;
                    //
                    File device = new File(DEV_DIRECTORY_NAME, fb.getName());
                    //
                    if (device.exists()) return device;
                }
                catch (IOException e)
                {
                    throw new IllegalStateException(e);
                }
            }
        }
        //
        throw new IllegalStateException("device not found");
    }
}
