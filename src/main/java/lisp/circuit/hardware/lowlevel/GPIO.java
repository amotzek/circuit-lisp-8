package lisp.circuit.hardware.lowlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
/*
 * Created by Andreas on 16.06.2016.
 */
public final class GPIO
{
    private static final String EXPORT_FILE_NAME = "/sys/class/gpio/export";
    private static final String UNEXPORT_FILE_NAME = "/sys/class/gpio/unexport";
    private static final String DIRECTORY_NAME = "/sys/class/gpio/gpio%s";
    private static final String DIRECTION_FILE_NAME = "/sys/class/gpio/gpio%s/direction";
    private static final String VALUE_FILE_NAME = "/sys/class/gpio/gpio%s/value";
    //
    private static HashMap<String, GPIO> gpiobyid = new HashMap<>();
    //
    public static GPIO getInstance(String id)
    {
        return gpiobyid.computeIfAbsent(id, GPIO::new);
    }
    //
    private final String id;
    //
    private GPIO(String id)
    {
        super();
        //
        this.id = id;
    }
    //
    public void export()
    {
        // Vorbedingung: sudo chmod 222 /sys/class/gpio/export /sys/class/gpio/unexport
        try (FileOutputStream out = new FileOutputStream(EXPORT_FILE_NAME, true))
        {
            echo(id, out);
            await(new File(String.format(DIRECTORY_NAME, id))); // dem Treiber Zeit geben, damit das gpio-Verzeichnis angelegt werden kann
        }
        catch (Exception e)
        {
            throw new IllegalStateException(id, e);
        }
    }
    //
    public void setDirection(String direction)
    {
        try (FileOutputStream out = new FileOutputStream(String.format(DIRECTION_FILE_NAME, id)))
        {
            echo(direction, out);
        }
        catch (IOException e)
        {
            throw new IllegalStateException(id, e);
        }
    }
    //
    public boolean readValue()
    {
        try (FileInputStream in = new FileInputStream(String.format(VALUE_FILE_NAME, id)))
        {
            int nextbyte = in.read();
            //
            if (nextbyte < 0) throw new IllegalStateException("end of file for gpio" + id);
            //
            if (nextbyte == '0') return false;
            //
            if (nextbyte == '1') return true;
            //
            throw new IllegalStateException("unexpected input " + nextbyte + " from gpio" + id);
        }
        catch (IOException e)
        {
            throw new IllegalStateException(id, e);
        }
    }
    //
    public void writeValue(boolean value)
    {
        try (FileOutputStream out = new FileOutputStream(String.format(VALUE_FILE_NAME, id)))
        {
            if (value)
            {
                echo("1", out);
            }
            else
            {
                echo("0", out);
            }
        }
        catch (IOException e)
        {
            throw new IllegalStateException(id, e);
        }
    }
    //
    public void unexport()
    {
        try (FileOutputStream out = new FileOutputStream(UNEXPORT_FILE_NAME))
        {
            echo(id, out);
        }
        catch (IOException e)
        {
            throw new IllegalStateException(id, e);
        }
    }
    //
    private static void echo(String line, OutputStream out) throws IOException
    {
        String linenl = line + '\n';
        byte[] bytes = linenl.getBytes("UTF-8");
        out.write(bytes);
    }
    //
    private static void await(File file) throws InterruptedException
    {
        while (!file.exists())
        {
            Thread.sleep(100L);
        }
    }
}