package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Array;
import lisp.CannotEvalException;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.lowlevel.FB;
import lisp.circuit.hardware.lowlevel.SenseHatFB;
import lisp.circuit.model.Output;
import java.util.Arrays;
import java.util.Iterator;
//
public final class FrameBufferOutput extends Output
{
    private final FB fb;
    private final byte[] pixels;
    //
    public FrameBufferOutput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            //
            switch (argumentstring)
            {
                case "sense-hat": fb = SenseHatFB.getInstance(); pixels = new byte[fb.size()]; return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create frame buffer output with argument %s", argument));
    }
    //
    @Override
    protected void valueChanged()
    {
        Sexpression value = getValue();
        //
        if (value == null)
        {
            clear();
            showPixels();
            //
            return;
        }
        //
        if (value instanceof Array)
        {
            Array array = (Array) value;
            clear();
            toPixels(array);
            showPixels();
            //
            return;
        }
        //
        if (value instanceof List)
        {
            List list = (List) value;
            clear();
            toPixels(list);
            showPixels();
            //
            return;
        }
        //
        throw new IllegalArgumentException(String.format("cannot set value %s", value));
    }
    //
    @Override
    public void postConstruct()
    {
        initialize();
        clear();
        showPixels();
    }
    //
    @Override
    public void preDestroy()
    {
        try
        {
            clear();
            showPixels();
        }
        catch (Exception ignore)
        {
        }
    }
    //
    private void initialize()
    {
        fb.initialize();
    }
    //
    private void clear()
    {
        Arrays.fill(pixels, (byte) 0);
    }
    //
    private void toPixels(Array array)
    {
        if (array.getDimensions().length != 2) throw new IllegalArgumentException(String.format("%s is not a two-dimensional array", array));
        //
        Iterator<List> keys = array.keyIterator();
        //
        while (keys.hasNext())
        {
            List position = keys.next();
            Sexpression value = array.get(position);
            setPixel(position, value);
        }
    }
    //
    private void toPixels(List list)
    {
        while (list != null)
        {
            List element = (List) list.first();
            List position = (List) element.first();
            List values = element.rest();
            setPixel(position, values);
            list = list.rest();
        }
    }
    //
    private void setPixel(List position, Sexpression value)
    {
        Rational x = (Rational) position.first();
        position = position.rest();
        Rational y = (Rational) position.first();
        //
        //
        try
        {
            if (value == null)
            {
                fb.setPixel(x.intValue(), y.intValue(), 0, pixels);
            }
            else if (value instanceof List)
            {
                List values = (List) value;
                //
                if (values.rest() == null)
                {
                    Rational w = (Rational) values.first();
                    fb.setPixel(x.intValue(), y.intValue(), w.intValue(), pixels);
                }
                else
                {
                    Rational r = (Rational) values.first();
                    values = values.rest();
                    Rational g = (Rational) values.first();
                    values = values.rest();
                    Rational b = (Rational) values.first();
                    fb.setPixel(x.intValue(), y.intValue(), r.intValue(), g.intValue(), b.intValue(), pixels);
                }
            }
            else if (value instanceof Rational)
            {
                Rational w = (Rational) value;
                fb.setPixel(x.intValue(), y.intValue(), w.intValue(), pixels);
            }
            else
            {
                throw new IllegalArgumentException(String.format("%s is not a pixel value", value));
            }
        }
        catch (CannotEvalException e)
        {
            throw new IllegalArgumentException(e);
        }
    }
    //
    private void showPixels()
    {
        fb.showPixels(pixels);
    }
}
