package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.lowlevel.IMU;
import lisp.circuit.hardware.lowlevel.LSM9DS1;
import lisp.circuit.model.Input;
/*
 * Created by Andreas on 24.05.2018.
 */
public final class InertialInput extends Input
{
    private final IMU imu;
    //
    public InertialInput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            //
            switch (argumentstring)
            {
                case "lsm9ds1": imu = LSM9DS1.getInstance(); return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create inertial input with argument %s", argument));
    }
    //
    @Override
    public Sexpression getValue()
    {
        int[] readings = imu.readValues();
        //
        if (readings.length == 1) return new Rational(readings[0]);
        //
        List list = null;
        //
        for (int reading : readings)
        {
            list = new List(new Rational(reading), list);
        }
        //
        return List.reverse(list);
    }
    //
    @Override
    public void postConstruct()
    {
        imu.initialize();
    }
    //
    @Override
    public void preDestroy()
    {
    }
}
