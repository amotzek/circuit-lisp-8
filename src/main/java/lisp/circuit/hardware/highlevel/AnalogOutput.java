package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.lowlevel.DAC;
import lisp.circuit.hardware.lowlevel.PCF8591;
import lisp.circuit.model.Output;
/*
 * Created by Andreas on 28.03.2018.
 */
public final class AnalogOutput extends Output
{
    private final DAC dac;
    //
    public AnalogOutput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            //
            switch (argumentstring)
            {
                case "pcf-8591": dac = PCF8591.getInstance(); return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create analog output with argument %s", argument));
    }
    //
    @Override
    protected void valueChanged()
    {
        Sexpression value = getValue();
        //
        if (value instanceof Rational)
        {
            Rational rationalvalue = (Rational) value;
            //
            try
            {
                int[] writings = new int[] { rationalvalue.intValue() };
                dac.writeValues(writings);
            }
            catch (CannotEvalException ignore)
            {
                throw new IllegalStateException("analog output needs an small integer value");
            }
        }
        else
        {
            throw new IllegalStateException("only numbers are supported as values for analog output");
        }
    }
    //
    @Override
    public void postConstruct()
    {
        dac.initialize();
    }
    //
    @Override
    public void preDestroy()
    {
    }
}
