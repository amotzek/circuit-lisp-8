package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.lowlevel.GPIO;
import lisp.circuit.model.Output;
/*
 * Created by Andreas on 28.03.2018.
 */
public final class DigitalOutput extends Output
{
    private final GPIO gpio;
    //
    public DigitalOutput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            //
            if (argumentstring.startsWith("gpio-"))
            {
                gpio = GPIO.getInstance(argumentstring.substring(5));
                //
                return;
            }
            //
            switch (argumentstring)
            {
                case "explorer-phat-output-1": gpio = GPIO.getInstance("6"); return;
                case "explorer-phat-output-2": gpio = GPIO.getInstance("12"); return;
                case "explorer-phat-output-3": gpio = GPIO.getInstance("13"); return;
                case "explorer-phat-output-4": gpio = GPIO.getInstance("16"); return;
                case "explorer-phat-motor-1+": gpio = GPIO.getInstance("19"); return;
                case "explorer-phat-motor-1-": gpio = GPIO.getInstance("20"); return;
                case "explorer-phat-motor-2+": gpio = GPIO.getInstance("21"); return;
                case "explorer-phat-motor-2-": gpio = GPIO.getInstance("26"); return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create digital output with argument %s", argument));
    }
    //
    @Override
    protected void valueChanged()
    {
        gpio.writeValue(getValue() != null);
    }
    //
    @Override
    public void postConstruct()
    {
        gpio.export();
        gpio.setDirection("out");
        gpio.writeValue(false);
    }
    //
    @Override
    public void preDestroy()
    {
        try
        {
            gpio.writeValue(false);
        }
        catch (Exception ignore)
        {
        }
        //
        try
        {
            gpio.unexport();
        }
        catch (Exception ignore)
        {
        }
    }
}
