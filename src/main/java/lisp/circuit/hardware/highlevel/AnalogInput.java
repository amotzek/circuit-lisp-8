package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.lowlevel.ADC;
import lisp.circuit.hardware.lowlevel.ADS1015;
import lisp.circuit.hardware.lowlevel.PCF8591;
import lisp.circuit.model.Input;
/*
 * Created by Andreas on 28.03.2018.
 */
public final class AnalogInput extends Input
{
    private final ADC adc;
    //
    public AnalogInput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            //
            switch (argumentstring)
            {
                case "pcf-8591": adc = PCF8591.getInstance(); return;
                case "explorer-phat-analog-1": case "ads-1015-ain0": adc = ADS1015.getInstance(0); return;
                case "explorer-phat-analog-2": case "ads-1015-ain1": adc = ADS1015.getInstance(1); return;
                case "explorer-phat-analog-3": case "ads-1015-ain2": adc = ADS1015.getInstance(2); return;
                case "explorer-phat-analog-4": case "ads-1015-ain3": adc = ADS1015.getInstance(3); return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create analog input with argument %s", argument));
    }
    //
    @Override
    public Sexpression getValue()
    {
        int[] readings = adc.readValues();
        //
        if (readings.length == 1) return new Rational(readings[0]);
        //
        List list = null;
        //
        for (int reading : readings)
        {
            list = new List(new Rational(reading), list);
        }
        //
        return List.reverse(list);
    }
    //
    @Override
    public void postConstruct()
    {
        adc.initialize();
    }
    //
    @Override
    public void preDestroy()
    {
    }
}
