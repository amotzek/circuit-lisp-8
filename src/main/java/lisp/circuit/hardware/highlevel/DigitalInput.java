package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.circuit.hardware.lowlevel.GPIO;
import lisp.circuit.model.Input;
/*
 * Created by Andreas on 28.03.2018.
 */
public final class DigitalInput extends Input
{
    private static final Symbol T = Symbol.createSymbol("t");
    //
    private final GPIO gpio;
    //
    public DigitalInput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            //
            if (argumentstring.startsWith("gpio-"))
            {
                gpio = GPIO.getInstance(argumentstring.substring(5));
                //
                return;
            }
            //
            switch (argumentstring)
            {
                case "explorer-phat-input-1": gpio = GPIO.getInstance("23"); return;
                case "explorer-phat-input-2": gpio = GPIO.getInstance("22"); return;
                case "explorer-phat-input-3": gpio = GPIO.getInstance("24"); return;
                case "explorer-phat-input-4": gpio = GPIO.getInstance("25"); return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create digital input with argument %s", argument));
    }
    //
    @Override
    public Sexpression getValue()
    {
        if (gpio.readValue()) return T;
        //
        return null;
    }
    //
    @Override
    public void postConstruct()
    {
        gpio.export();
        gpio.setDirection("in");
    }
    //
    @Override
    public void preDestroy()
    {
        try
        {
            gpio.unexport();
        }
        catch (Exception ignore)
        {
        }
    }
}
