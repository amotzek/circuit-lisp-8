package lisp.circuit.hardware.highlevel;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.*;
import lisp.circuit.hardware.lowlevel.*;
import lisp.circuit.model.Input;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.util.Map;
/*
 * Created by Andreas on 13.04.2018.
 */
public final class SerialInput extends Input
{
    private final SerialDevice device;
    private final LineConsumer consumer;
    //
    public SerialInput(Symbol name, Sexpression argument)
    {
        super(name);
        //
        if (argument instanceof Symbol)
        {
            Symbol argumentsymbol = (Symbol) argument;
            String argumentstring = argumentsymbol.getName();
            String osname = System.getProperty("os.name").toLowerCase();
            //
            if (osname.contains("windows 7"))
            {
                device = COM.getInstance(findSerial("COM3", "COM4"));
            }
            else if (osname.contains("windows"))
            {
                device = COM.getInstance(findSerial("COM10", "COM11"));
            }
            else
            {
                device = TTY.getInstance("ACM0");
            }
            //
            switch (argumentstring)
            {
                case "gps":
                    consumer = new GPS();
                    //
                    return;
                    //
                case "python-dictionary":
                    consumer = new PythonDictionary();
                    //
                    return;
            }
        }
        //
        throw new IllegalArgumentException(String.format("cannot create serial input with argument %s", argument));
    }
    //
    @Override
    public Sexpression getValue()
    {
        Object[] readings = consumer.readValues();
        //
        if (readings == null) return null;
        //
        if (readings.length == 1) return toSexpression(readings[0]);
        //
        return toList(readings);
    }
    //
    @SuppressWarnings("unchecked")
    private static Sexpression toSexpression(Object object)
    {
        if (object instanceof String) return new Chars((String) object);
        //
        if (object instanceof Integer) return new Rational((Integer) object);
        //
        if (object instanceof Long) return new Rational((Long) object);
        //
        if (object instanceof BigInteger) return new Rational((BigInteger) object);
        //
        if (object instanceof Double) return new Rational(object.toString());
        //
        if (object instanceof Map) return toRubyStyleObject((Map<String, Object>) object);
        //
        if (object instanceof Object[]) return toList((Object[]) object);
        //
        throw new IllegalStateException(String.format("cannot convert %s into an expression", object));
    }
    //
    private static RubyStyleObject toRubyStyleObject(Map<String, Object> map)
    {
        RubyStyleObject instance = new RubyStyleObject(null);
        //
        for (String key : map.keySet())
        {
            instance.put(Symbol.createSymbol(key), toSexpression(map.get(key)));
        }
        //
        return instance;
    }
    //
    private static List toList(Object[] array)
    {
        List list = null;
        //
        for (int i = array.length - 1; i >= 0; i--)
        {
            list = new List(toSexpression(array[i]), list);
        }
        //
        return list;
    }
    //
    @Override
    public void postConstruct()
    {
        device.open();
        device.readLinesInto(consumer);
    }
    //
    @Override
    public void preDestroy()
    {
        device.close();
    }
    //
    private static String findSerial(String... candidates)
    {
        for (String candidate : candidates)
        {
            try
            {
                RandomAccessFile file = new RandomAccessFile("\\\\.\\" + candidate, "r");
                file.close();
                //
                return candidate;
            }
            catch (IOException ignore)
            {
            }
        }
        //
        throw new IllegalStateException("serial not found");
    }
}
