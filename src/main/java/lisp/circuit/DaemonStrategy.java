package lisp.circuit;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.circuit.model.Task;

import java.io.File;
/*
 * Created by andreasm on 11.04.2018
 */
final class DaemonStrategy extends AbstractStrategy
{
    DaemonStrategy(File file)
    {
        super(file);
    }
    //
    @Override
    boolean hasConsoleActivity()
    {
        return false;
    }
    //
    @Override
    void onControllerEnded() throws InterruptedException
    {
        waitForChangedFile();
    }
    //
    @Override
    void onTaskRun(Task task)
    {
    }
    //
    @Override
    boolean shouldRepeat()
    {
        return true;
    }
}
