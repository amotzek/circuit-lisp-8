package lisp.circuit;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by Andreas on 27.03.2018.
 */
public final class App
{
    public static void main(String[] args)
    {
        Logger logger = Logger.getAnonymousLogger();
        //
        try
        {
            File file = new File(args[0]);
            AbstractStrategy strategy;
            //
            if (isInteractive(args))
            {
                strategy = new InteractiveStrategy(file);
            }
            else if (isRunOnce(args))
            {
                strategy = new RunOnceStrategy(file);
            }
            else
            {
                strategy = new DaemonStrategy(file);
            }
            //
            strategy.run();
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, "service stopped", e);
        }
    }
    //
    private static boolean isInteractive(String[] args)
    {
        for (int i = 1; i < args.length; i++)
        {
            if ("--interactive".equals(args[i])) return true;
        }
        //
        return false;
    }
    //
    private static boolean isRunOnce(String[] args)
    {
        for (int i = 1; i < args.length; i++)
        {
            if ("--once".equals(args[i])) return true;
        }
        //
        return false;
    }
}
