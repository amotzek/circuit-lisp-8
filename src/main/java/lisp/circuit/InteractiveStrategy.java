package lisp.circuit;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Sexpression;
import lisp.circuit.controller.Controller;
import lisp.circuit.model.Task;
import lisp.environment.Environment;
import lisp.parser.Parser;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;
/*
 * Created by andreasm on 11.04.2018
 */
final class InteractiveStrategy extends AbstractStrategy
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final BufferedReader reader;
    //
    InteractiveStrategy(File file)
    {
        super(file);
        //
        reader = new BufferedReader(new InputStreamReader(System.in));
    }
    //
    @Override
    boolean hasConsoleActivity() throws IOException
    {
        return reader.ready();
    }
    //
    @Override
    void onControllerEnded() throws IOException
    {
        Controller controller = getController();
        //
        if (controller == null) return;
        //
        logger.info("type (restart), (stop) or an expression");
        //
        for (String line = reader.readLine(); line != null; line = reader.readLine())
        {
            line = line.trim();
            //
            if (line.length() == 0) continue;
            //
            if (line.equals("(restart)")) break;
            //
            if (line.equals("(stop)")) throw new IOException("stop");
            //
            try
            {
                Environment environment = controller.getEnvironment();
                Parser parser = new Parser(line, 0);
                Sexpression expression = parser.getSexpression();
                Closure closure = new Closure(environment, expression);
                System.out.println(closure.eval());
            }
            catch (CannotEvalException e)
            {
                System.out.println(String.format("throw %s %s", e.getSymbol(), e.getSexpression()));
            }
        }
    }
    //
    @Override
    void onTaskRun(Task task)
    {
        logger.info(String.format("called %s", task.getContinuation()));
    }
    //
    @Override
    boolean shouldRepeat()
    {
        return true;
    }
}
