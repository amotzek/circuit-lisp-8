package lisp.circuit;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.circuit.controller.Controller;
import lisp.circuit.io.ControllerReader;
import lisp.circuit.model.Task;
import propertyset.VisitorException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;
/*
 * Created by andreasm on 11.04.2018
 */
abstract class AbstractStrategy
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final File file;
    private Controller controller;
    private long lastmodified;
    //
    AbstractStrategy(File file)
    {
        this.file = file;
    }
    //
    final Controller getController()
    {
        return controller;
    }
    //
    final void run() throws InterruptedException, IOException
    {
        do
        {
            if (readController())
            {
                runController();
                onControllerEnded();
            }
            else
            {
                waitForChangedFile();
            }
        }
        while (shouldRepeat());
    }
    //
    abstract boolean hasConsoleActivity() throws IOException;
    //
    abstract void onControllerEnded() throws IOException, InterruptedException;
    //
    abstract void onTaskRun(Task task);
    //
    abstract boolean shouldRepeat();
    //
    private boolean readController() throws IOException
    {
        try (FileInputStream stream = new FileInputStream(file))
        {
            ControllerReader reader = new ControllerReader();
            reader.readFrom(stream, "UTF-8");
            controller = reader.getController();
            lastmodified = file.lastModified();
            //
            return true;
        }
        catch (VisitorException e)
        {
            logger.warning(String.format("syntax exception %s", e.getMessage()));
        }
        catch (CannotEvalException e)
        {
            logger.warning(String.format("setup exception %s", e.getSexpression()));
        }
        //
        controller = null;
        lastmodified = file.lastModified();
        //
        return false;
    }
    //
    private void runController() throws IOException, InterruptedException
    {
        try
        {
            controller.postConstruct();
            //
            while (true)
            {
                if (hasConsoleActivity())
                {
                    logger.info("console active");
                    //
                    break;
                }
                //
                if (changedFile())
                {
                    logger.info(String.format("file %s was modified", file));
                    //
                    break;
                }
                //
                if (!controller.hasTasks())
                {
                    logger.warning("no tasks");
                    //
                    break;
                }
                //
                try
                {
                    Task task = controller.runTask();
                    //
                    if (task != null) onTaskRun(task);
                }
                catch (CannotEvalException e)
                {
                    logger.warning(String.format("runtime exception %s", e.getSexpression()));
                    //
                    break;
                }
            }
        }
        finally
        {
            controller.preDestroy();
        }
    }
    //
    final void waitForChangedFile() throws InterruptedException
    {
        controller = null;
        logger.info("wait for file change");
        //
        while (!changedFile())
        {
            Thread.sleep(3000L);
        }
    }
    //
    private boolean changedFile()
    {
        return lastmodified != file.lastModified();
    }
}
