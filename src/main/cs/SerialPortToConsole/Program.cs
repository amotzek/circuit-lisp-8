﻿using System;
using System.IO.Ports;
using System.Threading;

namespace SerialPortToConsole
{
    class Program
    {
        private static SerialPort port;

        static void Main(String[] args)
        {
            try
            {
                OpenPort(args[0]);
                Thread thread = new Thread(ToPort);
                thread.Start();
                FromPort();
                thread.Join();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
            }
        }

        private static void OpenPort(String portName)
        {
            port = new SerialPort(portName);
            port.ReadTimeout = 60000;
            port.WriteTimeout = 60000;
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.Open();
        }

        private static void FromPort()
        {
            while (port.IsOpen)
            {
                try
                {
                    String line = port.ReadLine();
                    Console.WriteLine(line);
                }
                catch (TimeoutException)
                {
                }
            }
        }

        private static void ToPort()
        {
            while (port.IsOpen)
            {
                try
                {
                    String line = Console.ReadLine();
                    //
                    if (line == null)
                    {
                        port.Close();
                        //
                        return;
                    }
                    //
                    port.WriteLine(line);
                }
                catch (TimeoutException)
                {
                }
            }
        }
    }
}
