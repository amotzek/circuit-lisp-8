## Circuit Lisp

Die aktuelle Version von Circuit Lisp kann mit

    curl https://bitbucket.org/amotzek/circuit-lisp-8/downloads/circuit-lisp-8.0-jar-with-dependencies.jar -L -o "Circuit_Lisp.jar"

heruntergeladen werden. Die heruntergeladene JAR-Datei verschiebt man ins Home-Verzeichnis (normalerweise /home/pi), wenn sie sich nicht schon dort befindet.

Im interaktiven Modus kann Circuit Lisp von der Kommandozeile gestartet werden mit

    sudo java -jar Circuit_Lisp.jar {Datei} --interactive

wobei anstatt des Platzhalters {Datei} der Name einer Datei angegeben wird, die auf das Programm einer Maschine verweist.

Wenn man eine Maschine dauerhaft betreiben will, kann man das Circuit Lisp als Dienst einrichten.


### Beispiel

Das folgende Beispiel lässt eine an GPIO 18 angeschlossene LED langsam blinken.

    (defoutput lamp digital gpio-18) #| Die Variable lamp steuert den digitalen Ausgang GPIO 18. |#

    (defproc off ()
      (progn
        (setq lamp nil) #| schaltet die LED aus |#
        (after 1000 on))) #| nach 1000 Millisekunden wird die Prozedur on aufgerufen |#

    (defproc on ()
      (progn
        (setq lamp t) #| schaltet die LED ein |#
        (after 1000 off))) #| nach 1 Sekunde wird die Prozedur off aufgerufen |#

    (now off) #| jetzt off aufrufen |#


Circuit Lisp hat außer den im Beispiel vorkommenden `defoutput`, `after` und `now` weitere spezielle Befehle, mit denen sich Hardware kontrollieren oder steuern lässt.

### definput

Mit (definput {name} {digital|analog} {gpio-##|pcf-8591}) kann die globale Variable {name} mit digitalen oder analogen Eingängen verknüpft werden. Beispielsweise verknüpft (definput switch digital gpio-18) die globale Variable switch mit GPIO 18. (definput sensors analog pcf-8591) verknüpft die Variable sensors mit den vier analogen Eingängen eines PCF 8591, der über I²C mit dem Raspberry Pi verbunden ist.

### defoutput

Mit (defoutput {name} {digital|analog|frame-buffer} {gpio-##|pcf-8591|sense-hat}) kann die globale Variable {name} mit digitalen oder analogen Ausgängen verknüpft werden. Außerdem können Anzeigegeräte wie LED-Matrixen oder OLED-Displays angesteuert werden. Mit (defoutput motor-1 digital gpio-19) wird die globale Variable motor-1 mit GPIO 19 verknüpft. (defoutput heating analog pcf-8591) verknüpft die globale Variable heating mit dem analogen Ausgang eines PCF 8591. Mit (defoutput pixels frame-buffer sense-hat) wird die Variable pixels mit der LED-Matrix des Sense Hat verknüpft.

### now

Die Auswertung von (now {Prozedur}) sorgt dafür, dass die Prozedur {Prozedur} sofort aufgerufen wird.

### after

Wenn (after {Zeitspanne} {Prozedur}) ausgewertet wird, sorgt das dafür, dass die Prozedur {Prozedur} nach der angegebenen Zeitspanne aufgerufen wird. Die Zeitspanne wird in Millisekunden gemessen. (after 0 {Prozedur}) bedeutet das Gleiche wie (now {Prozedur}).

### await-then

Mit (await-then {Testfunktion} {Prozedur}) wird die Funktion {Testfunktion} wiederholt aufgerufen, solange bis sie einen Wert ungleich nil als Resultat liefert. Dann wird die Prozedur {Prozedur} aufrufen.

### await-for-then

Ein Aufruf von (await-for-then {Testfunktion} {Zeitspanne} {Prozedur}) ruft die Testfunktion wiederholt auf. Wenn diese länger als die angegebene Zeitspanne einen Wert ungleich nil liefert, wird die Prozedur aufgerufen. (await-for-then {Testfunktion} 0 {Prozedur}) und (await-then {Testfunktion} {Prozedur}) haben den gleichen Effekt.

### priority-now, priority-after, priority-await-then und priority-await-for-then

Diese Prozeduren erwartet ein Argument mehr, als ihre Gegenstücke ohne Priorität. Die Priorität ist eine Ganzzahl und muss als erstes Argument angegeben werden. (now {Prozedur}) und (priority-now 0 {Prozedur}) sind gleichbedeutend. Analoges gilt für die anderen Befehle.

### only-one-of

Der Befehl only-one-of wird benutzt, um zu deklarieren, dass nur eine der übergebenen Aufgaben - die, die als erste fällig wird - ausgeführt wird. Beispielsweise bedeutet (only-one-of (list (after 3000 on-timeout) (await-then test task))), dass die Prozedur task aufgerufen wird, wenn die Funktion test während der nächsten drei Sekunden einmal einen Wert ungleich nil liefert. Ist das nicht der Fall, wird die Prozedur on-timeout aufgerufen.

### publish-message

Wird (publish-message {Broker} {Client Id} {Username} {Passwort} {Topic} {Message} {Retain}) ausgeführt, dann wird die Message in eine JSON-Repräsentation umgewandelt und an den MQTT Broker übergeben. Die Client Id, der Username und das Passwort werden zum Aufbau der Session benutzt.

### log-message

(log-message {Message}) schreibt eine Textrepräsentation der Message ins Logfile.
